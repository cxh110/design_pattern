---
noteId: "ce30359089c511ea8714937ff2169fd2"
tags: []

---

## 访问者模式（Visitor模式）
在现实生活中，有些集合对象中存在多种不同的元素，且每种元素也存在多种不同的访问者和处理方式。例如，公园中存在多个景点，也存在多个游客，不同的游客对同一个景点的评价可能不同；医院医生开的处方单中包含多种药元素，査看它的划价员和药房工作人员对它的处理方式也不同，划价员根据处方单上面的药品名和数量进行划价，药房工作人员根据处方单的内容进行抓药。

这样的例子还有很多，例如，电影或电视剧中的人物角色，不同的观众对他们的评价也不同；还有顾客在商场购物时放在“购物车”中的商品，顾客主要关心所选商品的性价比，而收银员关心的是商品的价格和数量。

这些被处理的数据元素相对稳定而访问方式多种多样的数据结构，如果用“访问者模式”来处理比较方便。访问者模式能把处理方法从数据结构中分离出来，并可以根据需要增加新的处理方法，且不用修改原来的程序代码与数据结构，这提高了程序的扩展性和灵活性。

### 模式的定义与特点
访问者（Visitor）模式的定义：将作用于某种数据结构中的各元素的操作分离出来封装成独立的类，使其在不改变数据结构的前提下可以添加作用于这些元素的新的操作，为数据结构中的每个元素提供多种访问方式。它将对数据的操作与数据结构进行分离，是行为类模式中最复杂的一种模式。

访问者（Visitor）模式是一种对象行为型模式，其主要优点如下。
- 扩展性好。能够在不修改对象结构中的元素的情况下，为对象结构中的元素添加新的功能。
- 复用性好。可以通过访问者来定义整个对象结构通用的功能，从而提高系统的复用程度。
- 灵活性好。访问者模式将数据结构与作用于结构上的操作解耦，使得操作集合可相对自由地演化而不影响系统的数据结构。
- 符合单一职责原则。访问者模式把相关的行为封装在一起，构成一个访问者，使每一个访问者的功能都比较单一。

访问者（Visitor）模式的主要缺点如下。
- 增加新的元素类很困难。在访问者模式中，每增加一个新的元素类，都要在每一个具体访问者类中增加相应的具体操作，这违背了“开闭原则”。
- 破坏封装。访问者模式中具体元素对访问者公布细节，这破坏了对象的封装性。
- 违反了依赖倒置原则。访问者模式依赖了具体类，而没有依赖抽象类。

###  模式的结构与实现
访问者（Visitor）模式实现的关键是如何将作用于元素的操作分离出来封装成独立的类，其基本结构与实现方法如下。
1. 模式的结构
访问者模式包含以下主要角色。
- 抽象访问者（Visitor）角色：定义一个访问具体元素的接口，为每个具体元素类对应一个访问操作 visit() ，该操作中的参数类型标识了被访问的具体元素。
- 具体访问者（ConcreteVisitor）角色：实现抽象访问者角色中声明的各个访问操作，确定访问者访问一个元素时该做什么。
- 抽象元素（Element）角色：声明一个包含接受操作 accept() 的接口，被接受的访问者对象作为 accept() 方法的参数。
- 具体元素（ConcreteElement）角色：实现抽象元素角色提供的 accept() 操作，其方法体通常都是 visitor.visit(this) ，另外具体元素中可能还包含本身业务逻辑的相关操作。
- 对象结构（Object Structure）角色：是一个包含元素角色的容器，提供让访问者对象遍历容器中的所有元素的方法，通常由 List、Set、Map 等聚合类实现。

![访问者（Visitor）模式的结构图](访问者Visitor模式的结构图.gif)

```java
    package visitor;
    import java.util.*;
    public class VisitorPattern
    {
        public static void main(String[] args)
        {
            ObjectStructure os=new ObjectStructure();
            os.add(new ConcreteElementA());
            os.add(new ConcreteElementB());
            Visitor visitor=new ConcreteVisitorA();
            os.accept(visitor);
            System.out.println("------------------------");
            visitor=new ConcreteVisitorB();
            os.accept(visitor);
        }
    }
    //抽象访问者
    interface Visitor
    {
        void visit(ConcreteElementA element);
        void visit(ConcreteElementB element);
    }
    //具体访问者A类
    class ConcreteVisitorA implements Visitor
    {
        public void visit(ConcreteElementA element)
        {
            System.out.println("具体访问者A访问-->"+element.operationA());
        }
        public void visit(ConcreteElementB element)
        {
            System.out.println("具体访问者A访问-->"+element.operationB());
        }
    }
    //具体访问者B类
    class ConcreteVisitorB implements Visitor
    {
        public void visit(ConcreteElementA element)
        {
            System.out.println("具体访问者B访问-->"+element.operationA());
        }
        public void visit(ConcreteElementB element)
        {
            System.out.println("具体访问者B访问-->"+element.operationB());
        }
    }
    //抽象元素类
    interface Element
    {
        void accept(Visitor visitor);
    }
    //具体元素A类
    class ConcreteElementA implements Element
    {
        public void accept(Visitor visitor)
        {
            visitor.visit(this);
        }
        public String operationA()
        {
            return "具体元素A的操作。";
        }
    }
    //具体元素B类
    class ConcreteElementB implements Element
    {
        public void accept(Visitor visitor)
        {
            visitor.visit(this);
        }
        public String operationB()
        {
            return "具体元素B的操作。";
        }
    }
    //对象结构角色
    class ObjectStructure
    {   
        private List<Element> list=new ArrayList<Element>();   
        public void accept(Visitor visitor)
        {
            Iterator<Element> i=list.iterator();
            while(i.hasNext())
            {
                ((Element) i.next()).accept(visitor);
            }      
        }
        public void add(Element element)
        {
            list.add(element);
        }
        public void remove(Element element)
        {
            list.remove(element);
        }
    }
```
```
具体访问者A访问-->具体元素A的操作。
具体访问者A访问-->具体元素B的操作。
------------------------
具体访问者B访问-->具体元素A的操作。
具体访问者B访问-->具体元素B的操作。
```
###  模式的应用实例
【例1】利用“访问者（Visitor）模式”模拟艺术公司与造币公司的功能。

分析：艺术公司利用“铜”可以设计出铜像，利用“纸”可以画出图画；造币公司利用“铜”可以印出铜币，利用“纸”可以印出纸币（点此下载运行该程序后所要显示的图片）。对“铜”和“纸”这两种元素，两个公司的处理方法不同，所以该实例用访问者模式来实现比较适合。

首先，定义一个公司（Company）接口，它是抽象访问者，提供了两个根据纸（Paper）或铜（Cuprum）这两种元素创建作品的方法；再定义艺术公司（ArtCompany）类和造币公司（Mint）类，它们是具体访问者，实现了父接口的方法；然后，定义一个材料（Material）接口，它是抽象元素，提供了 accept（Company visitor）方法来接受访问者（Company）对象访问；再定义纸（Paper）类和铜（Cuprum）类，它们是具体元素类，实现了父接口中的方法；最后，定义一个材料集（SetMaterial）类，它是对象结构角色，拥有保存所有元素的容器 List，并提供让访问者对象遍历容器中的所有元素的 accept（Company visitor）方法；客户类设计成窗体程序，它提供材料集（SetMaterial）对象供访问者（Company）对象访问，实现了 ItemListener 接口，处理用户的事件请求。图 2 所示是其结构图。
![艺术公司与造币公司的结构图](艺术公司与造币公司的结构图.gif)
```java
    package visitor;
    import java.awt.event.*;
    import java.util.*;
    import javax.swing.*;
    public class VisitorProducer
    {
        public static void main(String[] args)
        {
            new MaterialWin();       
        }
    }
    //窗体类
    class MaterialWin extends JFrame implements ItemListener
    {
        private static final long serialVersionUID=1L;   
        JPanel CenterJP;
        SetMaterial os;    //材料集对象
        Company visitor1,visitor2;    //访问者对象
        String[] select;
        MaterialWin()
        {
            super("利用访问者模式设计艺术公司和造币公司");
            JRadioButton Art;
            JRadioButton mint;           
            os=new SetMaterial();     
            os.add(new Cuprum());
            os.add(new Paper());
            visitor1=new ArtCompany();//艺术公司
            visitor2=new Mint(); //造币公司      
            this.setBounds(10,10,750,350);            
            this.setResizable(false);       
            CenterJP=new JPanel();       
            this.add("Center",CenterJP);      
            JPanel SouthJP=new JPanel();
            JLabel yl=new JLabel("原材料有：铜和纸，请选择生产公司：");
            Art=new JRadioButton("艺术公司",true);
            mint=new JRadioButton("造币公司");
            Art.addItemListener(this);
            mint.addItemListener(this);       
            ButtonGroup group=new ButtonGroup();
            group.add(Art);
            group.add(mint);
            SouthJP.add(yl);
            SouthJP.add(Art);
            SouthJP.add(mint);
            this.add("South",SouthJP);       
            select=(os.accept(visitor1)).split(" ");    //获取产品名
            showPicture(select[0],select[1]);    //显示产品
        }
        //显示图片
        void showPicture(String Cuprum,String paper)
        {
            CenterJP.removeAll();    //清除面板内容
            CenterJP.repaint();    //刷新屏幕
            String FileName1="src/visitor/Picture/"+Cuprum+".jpg";
            String FileName2="src/visitor/Picture/"+paper+".jpg";
            JLabel lb=new JLabel(new ImageIcon(FileName1),JLabel.CENTER);
            JLabel rb=new JLabel(new ImageIcon(FileName2),JLabel.CENTER);
            CenterJP.add(lb);
            CenterJP.add(rb);
            this.setVisible(true);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);            
        }
        @Override
        public void itemStateChanged(ItemEvent arg0)
        {
            JRadioButton jc=(JRadioButton) arg0.getSource();
            if (jc.isSelected())
            {
                if (jc.getText()=="造币公司")
                {
                    select=(os.accept(visitor2)).split(" ");
                }
                else
                {            
                    select=(os.accept(visitor1)).split(" ");
                }
                showPicture(select[0],select[1]);    //显示选择的产品
            }    
        }
    }
    //抽象访问者:公司
    interface Company
    {
        String create(Paper element);
        String create(Cuprum element);
    }
    //具体访问者：艺术公司
    class ArtCompany implements Company
    {
        public String create(Paper element)
        {
            return "讲学图";
        }
        public String create(Cuprum element)
        {
            return "朱熹铜像";
        }
    }
    //具体访问者：造币公司
    class Mint implements Company
    {
        public String create(Paper element)
        {
            return "纸币";
        }
        public String create(Cuprum element)
        {
            return "铜币";
        }
    }
    //抽象元素：材料
    interface Material
    {
        String accept(Company visitor);
    }
    //具体元素：纸
    class Paper implements Material
    {
        public String accept(Company visitor)
        {
            return(visitor.create(this));
        }
    }
    //具体元素：铜
    class Cuprum implements Material
    {
        public String accept(Company visitor)
        {
            return(visitor.create(this));
        }
    }
    //对象结构角色:材料集
    class SetMaterial
    {   
        private List<Material> list=new ArrayList<Material>();   
        public String accept(Company visitor)
        {
            Iterator<Material> i=list.iterator();
            String tmp="";
            while(i.hasNext())
            {
                tmp+=((Material) i.next()).accept(visitor)+" ";
            }
            return tmp; //返回某公司的作品集     
        }
        public void add(Material element)
        {
            list.add(element);
        }
        public void remove(Material element)
        {
            list.remove(element);
        }
    }
```
![艺术公司设计的产品](艺术公司设计的产品.jpg)
![造币公司生产的货币](造币公司生产的货币.jpg)

###  模式的应用场景
通常在以下情况可以考虑使用访问者（Visitor）模式。
- 对象结构相对稳定，但其操作算法经常变化的程序。
- 对象结构中的对象需要提供多种不同且不相关的操作，而且要避免让这些操作的变化影响对象的结构。
- 对象结构包含很多类型的对象，希望对这些对象实施一些依赖于其具体类型的操作。

### 模式的扩展
访问者（Visitor）模式是使用频率较高的一种设计模式，它常常同以下两种设计模式联用。

(1)与“迭代器模式”联用。因为访问者模式中的“对象结构”是一个包含元素角色的容器，当访问者遍历容器中的所有元素时，常常要用迭代器。如【例1】中的对象结构是用 List 实现的，它通过 List 对象的 Itemtor() 方法获取迭代器。如果对象结构中的聚合类没有提供迭代器，也可以用迭代器模式自定义一个。

(2)访问者（Visitor）模式同“组合模式”联用。因为访问者（Visitor）模式中的“元素对象”可能是叶子对象或者是容器对象，如果元素对象包含容器对象，就必须用到组合模式，其结构图如图 4 所示。
![包含组合模式的访问者模式的结构图](包含组合模式的访问者模式的结构图.jpg)

---

## CPP

### Visitor 模式
在面向对象系统的开发和设计过程，经常会遇到一种情况就是需求变更（ RequirementChanging）， 经常我们做好的一个设计、实现了一个系统原型，咱们的客户又会有了新的需求。 我们又因此不得不去修改已有的设计， 最常见就是解决方案就是给已经设计、 实现好的类添加新的方法去实现客户新的需求， 这样就陷入了设计变更的梦魇： 不停地打补丁， 其带来的后果就是设计根本就不可能封闭、编译永远都是整个系统代码。
Visitor 模式则提供了一种解决方案：将更新（变更）封装到一个类中（访问操作），并由待更改类提供一个接收接口，则可达到效果。

Visitor 模式在不破坏类的前提下， 为类提供增加新的新操作。 Visitor 模式的关键是双分派（ Double-Dispatch） 的技术【注释 1】。 C++语言支持的是单分派。在 Visitor 模式中 Accept（）操作是一个双分派的操作。具体调用哪一个具体的 Accept（）操作，有两个决定因素： 1） Element 的类型。因为 Accept（）是多态的操作，需要具体的 Element 类型的子类才可以决定到底调用哪一个 Accept（）实现； 2） Visitor 的类型。Accept（）操作有一个参数（ Visitor* vis），要决定了实际传进来的 Visitor 的实际类别才可以决定具体是调用哪个 VisitConcrete（）实现

### 实现
```cpp
// Visitor.h
#ifndef _VISITOR_H_
#define _VISITOR_H_
class ConcreteElementA;
class ConcreteElementB;
class Element;
class Visitor {
 public:
  virtual ~Visitor();
  virtual void VisitConcreteElementA(Element* elm) = 0;
  virtual void VisitConcreteElementB(Element* elm) = 0;

 protected:
  Visitor();

 private:
};
class ConcreteVisitorA : public Visitor {
 public:
  ConcreteVisitorA();
  virtual ~ConcreteVisitorA();
  virtual void VisitConcreteElementA(Element* elm);
  virtual void VisitConcreteElementB(Element* elm);

 protected:
 private:
};
class ConcreteVisitorB : public Visitor {
 public:
  ConcreteVisitorB();
  virtual ~ConcreteVisitorB();
  virtual void VisitConcreteElementA(Element* elm);
  virtual void VisitConcreteElementB(Element* elm);

 protected:
 private:
};
#endif  //~_VISITOR_H_
```
```cpp
// Visitor.cpp
#include <iostream>

#include "Element.h"
#include "Visitor.h"
using namespace std;
Visitor::Visitor() {}
Visitor::~Visitor() {}
ConcreteVisitorA::ConcreteVisitorA() {}
ConcreteVisitorA::~ConcreteVisitorA() {}
void ConcreteVisitorA::VisitConcreteElementA(Ele ment* elm) {
  cout << "i will visit ConcreteElementA... " << endl;
}
void ConcreteVisitorA::VisitConcreteElementB(Ele ment* elm) {
  cout << "i will visit ConcreteElementB... " << endl;
}
ConcreteVisitorB::ConcreteVisitorB() {}
ConcreteVisitorB::~ConcreteVisitorB() {}
void ConcreteVisitorB::VisitConcreteElementA(Ele ment* elm) {
  cout << "i will visit ConcreteElementA... " << endl;
}
void ConcreteVisitorB::VisitConcreteElementB(El ement* elm) {
  cout << "i will visit ConcreteElementB... " << endl;
}
```
```cpp
// Element.h
#ifndef _ELEMENT_H_
#define _ELEMENT_H_
class Visitor;
class Element {
 public:
  virtual ~Element();
  virtual void Accept(Visitor* vis) = 0;

 protected:
  Element();

 private:
};
class ConcreteElementA : public Element {
 public:
  ConcreteElementA();
  ~ConcreteElementA();
  void Accept(Visitor* vis);

 protected:
 private:
};
class ConcreteElementB : public Element {
 public:
  ConcreteElementB();
  ~ConcreteElementB();
  void Accept(Visitor* vis);

 protected:
 private:
};
#endif  //~_ELEMENT_H_
```
```cpp
// Element.cpp
#include <iostream>

#include "Element.h"
#include "Visitor.h"
using namespace std;
Element::Element() {}
Element::~Element() {}
void Element::Accept(Visitor* vis) {}
ConcreteElementA::ConcreteElementA() {}
ConcreteElementA::~ConcreteElementA() {}
void ConcreteElementA::Accept(Visitor* vis) {
  vis->VisitConcreteElementA(this);
  cout << "visiting ConcreteElementA... " << endl;
}
ConcreteElementB::ConcreteElementB() {}
ConcreteElementB::~ConcreteElementB() {}
void ConcreteElementB::Accept(Visitor* vis) {
  cout << "visiting ConcreteElementB... " << endl;
  vis->VisitConcreteElementB(this);
}
```
```cpp
#include <iostream>

#include "Element.h"
#include "Visitor.h"
using namespace std;
int main(int argc, char* argv[]) {
  Visitor* vis = new ConcreteVisitorA();
  Element* elm = new ConcreteElementA();
  elm->Accept(vis);
  return 0;
}
```

Visitor 模式的实现过程中有以下的地方要注意：
1） Visitor 类中的 Visit（）操作的实现。
- 这里我们可以向 Element 类仅仅提供一个接口 Visit（），而在 Accept（）实现中具体调用哪一个 Visit（） 操作则通过函数重载（ overload） 的方式实现： 我们提供 Visit（） 的两个重载版本 
a） Visi（t ConcreteElementA* elmA）， 
b） Visi（t ConcreteElementB*elmB）。
- 在 C++中我们还可以通过 RTTI（运行时类型识别： Runtime type identification）来实现， 即我们只提供一个 Visit（） 函数体， 传入的参数为 Element*型别参数 ，然后用 RTTI 决定具体是哪一类的 ConcreteElement 参数，再决定具体要对哪个具体类施加什么样的具体操作。【注释 2】 RTTI 给接口带来了简单一致性， 但是付出的代价是时间（ RTTI 的实现）和代码的 Hard 编码（要进行强制转换）

有时候我们需要为 Element 提供更多的修改， 这样我们就可以通过为 Element 提供一系列的Visitor 模式可以使得 Element 在不修改自己的同时增加新的操作， 但是这也带来了至少以下的两个显著问题：
1） 破坏了封装性。 Visitor 模式要求 Visitor 可以从外部修改 Element 对象的状态， 这一般通过两个方式来实现： a） Element 提供足够的 public 接口， 使得 Visitor 可以通过调用这些接口达到修改 Element 状态的目的； b） Element 暴露更多的细节给 Visitor，或者让 Element 提供 public 的实现给 Visitor（当然也给了系统中其他的对象），或者将 Visitor 声明为 Element 的 friend 类， 仅将细节暴露给 Visitor。 但是无论那种情况，特别是后者都将是破坏了封装性原则（实际上就是 C++的 friend 机制得到了很多的面向对象专家的诟病）。
2） ConcreteElement 的扩展很困难：每增加一个 Element 的子类， 就要修改 Visitor 的接口，使得可以提供给这个新增加的子类的访问机制。从上面我们可以看到，或者增加一个用于处理新增类的 Visit（） 接口， 或者重载一个处理新增类的 Visit（） 操作， 或者要修改 RTTI 方式实现的 Visi（） t 实现。 无论那种方式都给扩展新的 Element子类带来了困难。


