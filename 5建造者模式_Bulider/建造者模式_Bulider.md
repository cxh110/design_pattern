---
noteId: "31825d8082ff11eab28dab27f2a3be8b"
tags: []

---

## 建造者模式（Bulider模式）详解
在软件开发过程中有时需要创建一个复杂的对象，这个复杂对象通常由多个子部件按一定的步骤组合而成。例如，计算机是由 OPU、主板、内存、硬盘、显卡、机箱、显示器、键盘、鼠标等部件组装而成的，采购员不可能自己去组装计算机，而是将计算机的配置要求告诉计算机销售公司，计算机销售公司安排技术人员去组装计算机，然后再交给要买计算机的采购员。
生活中这样的例子很多，如游戏中的不同角色，其性别、个性、能力、脸型、体型、服装、发型等特性都有所差异；还有汽车中的方向盘、发动机、车架、轮胎等部件也多种多样；每封电子邮件的发件人、收件人、主题、内容、附件等内容也各不相同。
以上所有这些产品都是由多个部件构成的，各个部件可以灵活选择，但其创建步骤都大同小异。这类产品的创建无法用前面介绍的工厂模式描述，只有建造者模式可以很好地描述该类产品的创建。
### 模式的定义与特点
建造者（Builder）模式的定义：指将一个复杂对象的构造与它的表示分离，使同样的构建过程可以创建不同的表示，这样的设计模式被称为建造者模式。它是将一个复杂的对象分解为多个简单的对象，然后一步一步构建而成。它将变与不变相分离，即产品的组成部分是不变的，但每一部分是可以灵活选择的
该模式的主要优点如下：
    各个具体的建造者相互独立，有利于系统的扩展。
    客户端不必知道产品内部组成的细节，便于控制细节风险

其缺点如下：
    产品的组成部分必须相同，这限制了其使用范围。
    如果产品的内部变化复杂，该模式会增加很多的建造者类。

建造者（Builder）模式和工厂模式的关注点不同：建造者模式注重零部件的组装过程，而工厂方法模式更注重零部件的创建过程，但两者可以结合使用。

### 模式的结构与实现
建造者（Builder）模式由产品、抽象建造者、具体建造者、指挥者等 4 个要素构成，现在我们来分析其基本结构和实现方法。
1. 模式的结构
建造者（Builder）模式的主要角色如下。

> 产品角色（Product）：它是包含多个组成部件的复杂对象，由具体建造者来创建其各个滅部件。
> 抽象建造者（Builder）：它是一个包含创建产品各个子部件的抽象方法的接口，通常还包含一个返回复杂产品的方法 getResult()。
> 具体建造者(Concrete Builder）：实现 Builder 接口，完成复杂产品的各个部件的具体创建方法。
> 指挥者（Director）：它调用建造者对象中的部件构造与装配方法完成复杂对象的创建，在指挥者中不涉及具体产品的信息。
![建造者模式的结构图](建造者模式的结构图.gif)

2. 模式的实现
(1) 产品角色：包含多个组成部件的复杂对象。 
```java
    class Product
    {
        private String partA;
        private String partB;
        private String partC;
        public void setPartA(String partA)
        {
            this.partA=partA;
        }
        public void setPartB(String partB)
        {
            this.partB=partB;
        }
        public void setPartC(String partC)
        {
            this.partC=partC;
        }
        public void show()
        {
            //显示产品的特性
        }
    }
```
(2) 抽象建造者：包含创建产品各个子部件的抽象方法。 
```java
    abstract class Builder
    {
        //创建产品对象
        protected Product product=new Product();
        public abstract void buildPartA();
        public abstract void buildPartB();
        public abstract void buildPartC();
        //返回产品对象
        public Product getResult()
        {
            return product;
        }
    }
```
(3) 具体建造者：实现了抽象建造者接口。 
```java
    public class ConcreteBuilder extends Builder
    {
        public void buildPartA()
        {
            product.setPartA("建造 PartA");
        }
        public void buildPartB()
        {
            product.setPartA("建造 PartB");
        }
        public void buildPartC()
        {
            product.setPartA("建造 PartC");
        }
    }
```
(4) 指挥者：调用建造者中的方法完成复杂对象的创建。 
```java
    class Director
    {
        private Builder builder;
        public Director(Builder builder)
        {
            this.builder=builder;
        }
        //产品构建与组装方法
        public Product construct()
        {
            builder.buildPartA();
            builder.buildPartB();
            builder.buildPartC();
            return builder.getResult();
        }
    }
```
(5) 客户类
```java
    public class Client
    {
        public static void main(String[] args)
        {
            Builder builder=new ConcreteBuilder();
            Director director=new Director(builder);
            Product product=director.construct();
            product.show();
        }
    }
```
### 模式的应用实例
【例1】用建造者（Builder）模式描述客厅装修。
分析：客厅装修是一个复杂的过程，它包含墙体的装修、电视机的选择、沙发的购买与布局等。客户把装修要求告诉项目经理，项目经理指挥装修工人一步步装修，最后完成整个客厅的装修与布局，所以本实例用建造者模式实现比较适合。
这里客厅是产品，包括墙、电视和沙发等组成部分。具体装修工人是具体建造者，他们负责装修与墙、电视和沙发的布局。项目经理是指挥者，他负责指挥装修工人进行装修。
另外，客厅类中提供了 show() 方法，可以将装修效果图显示出来（点此下载装修效果图的图片）。客户端程序通过对象生成器类 ReadXML 读取 XML 配置文件中的装修方案数据（点此下载 XML 文件），调用项目经理进行装修。其类图如图 2 所示
![客厅装修的结构图](客厅装修的结构图.gif)

```java
    package Builder;
    import java.awt.*;
    import javax.swing.*;
    public class ParlourDecorator
    {
        public static void main(String[] args)
        {
            try
            {
                Decorator d;
                d=(Decorator) ReadXML.getObject();
                ProjectManager m=new ProjectManager(d);       
                Parlour p=m.decorate();
                p.show();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    //产品：客厅
    class Parlour
    {
        private String wall;    //墙
        private String TV;    //电视
        private String sofa;    //沙发  
        public void setWall(String wall)
        {
            this.wall=wall;
        }
        public void setTV(String TV)
        {
            this.TV=TV;
        }
        public void setSofa(String sofa)
        {
            this.sofa=sofa;
        }   
        public void show()
        {
            JFrame jf=new JFrame("建造者模式测试");
            Container contentPane=jf.getContentPane();
            JPanel p=new JPanel();   
            JScrollPane sp=new JScrollPane(p);  
            String parlour=wall+TV+sofa;
            JLabel l=new JLabel(new ImageIcon("src/"+parlour+".jpg"));
            p.setLayout(new GridLayout(1,1));
            p.setBorder(BorderFactory.createTitledBorder("客厅"));
            p.add(l);
            contentPane.add(sp,BorderLayout.CENTER);       
            jf.pack();  
            jf.setVisible(true);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }   
    }
    //抽象建造者：装修工人
    abstract class Decorator
    {
        //创建产品对象
        protected  Parlour product=new Parlour();
        public  abstract void buildWall();
        public  abstract void buildTV();
        public  abstract void buildSofa();
        //返回产品对象
        public  Parlour getResult()
        {
            return  product;
        }
    }
    //具体建造者：具体装修工人1
    class ConcreteDecorator1  extends Decorator
    {
        public void buildWall()
        {
            product.setWall("w1");
        }
        public void buildTV()
        {
            product.setTV("TV1");
        }
        public void buildSofa()
        {
            product.setSofa("sf1");
        }
    }
    //具体建造者：具体装修工人2
    class ConcreteDecorator2 extends Decorator
    {
        public void buildWall()
        {
            product.setWall("w2");
          }
          public void buildTV()
          {
              product.setTV("TV2");
          }
          public void buildSofa()
          {
              product.setSofa("sf2");
          }
    }
    //指挥者：项目经理
    class ProjectManager
    {
        private Decorator builder;
        public ProjectManager(Decorator builder)
        {
              this.builder=builder;
        }
        //产品构建与组装方法
        public Parlour decorate()
        {
              builder.buildWall();
            builder.buildTV();
            builder.buildSofa();
            return builder.getResult();
        }
    }
```

```java
    package Builder;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML
    {
        public static Object getObject()
        {
            try
            {
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/Builder/config.xml"));
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=nl.item(0).getFirstChild();
                String cName="Builder."+classNode.getNodeValue();
                System.out.println("新类名："+cName);
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
             }  
             catch(Exception e)
             {
                       e.printStackTrace();
                       return null;
             }
        }
    }
```
![客厅装修的运行结果](客厅装修的运行结果.jpg)

### 模式的应用场景
建造者（Builder）模式创建的是复杂对象，其产品的各个部分经常面临着剧烈的变化，但将它们组合在一起的算法却相对稳定，所以它通常在以下场合使用。

    创建的对象较复杂，由多个部件构成，各部件面临着复杂的变化，但构件间的建造顺序是稳定的。
    创建复杂对象的算法独立于该对象的组成部分以及它们的装配方式，即产品的构建过程和最终的表示是独立的

### 模式的扩展
建造者（Builder）模式在应用过程中可以根据需要改变，如果创建的产品种类只有一种，只需要一个具体建造者，这时可以省略掉抽象建造者，甚至可以省略掉指挥者角色。

## CPP
本章主要说明建造者模式，该设计模式主要意图是：将一个复杂对象的构建与它的表示分离，使同样的构建过程可以创建不同的表示。
### 主要参与者
该设计模式的参与者有四个，分别是：

> 1. Builder 抽象构造者，创造Product各个部件的抽象接口；
> 2. ConcreteBuilder 具体构造者，创造Product各个部件的具体实现接口；
> 3. Director 指导者，该类引导Builder来实现一个产品构造的成体流程；
> 4. Product 产品，表示被构造的复杂对象；

### 实施流程与具体实现
总体流程
> 1. 客户创建Director对象，并通过Director对想要的Builder对象配置；
> 2. 生成产品部件；
> 3. 返回各部件组合成的产品总成

### 具体实现代码
```cpp
#include <string>
#include <stdio.h>
using namespace std;

//产品基类
class Car
{
public:
    //获取产品部件函数
    string GetEngine() {return m_engine;}
    string GetGearBox() {return m_gearbox;}
    string GetChassis() {return m_chassis;}
    //设置产品部件函数
    void SetEngine(string engine){m_engine = engine;
                                 printf("Creat %s.\n",engine.c_str());}
    void SetGearBox(string gearbox){m_gearbox = gearbox;
                                   printf("Creat %s.\n",gearbox.c_str());}
    void SetChassis(string chassis){m_chassis = chassis;
                                   printf("Creat %s.\n",chassis.c_str());}
private:
    string m_engine;
    string m_gearbox;
    string m_chassis;
};
```

产品子类(Product)
```cpp
//宝马车产品
class BMWCar: public Car
{
public:
    BMWCar(){printf("Start build BMW.\n");}

};

//奔驰车产品
class BenzCar: public Car
{
public:
    BenzCar(){printf("Start build Benz.\n");}
};

```
抽象建造者类（Builder）

```cpp
//抽象建造者类
class CarBuilder
{
public:
    virtual Car *BuildCar(){}		//造整车
    virtual void BuildEngine(){}	//造引擎
    virtual void BuildGearBox(){}	//造变速箱
    virtual void BuildChassis(){}	//造底盘
};

```
具体建造者类（ConcreteBuilder ）
```cpp
//具体宝马建造者类
class BMWCarBuilder: public CarBuilder
{
public:
    BMWCarBuilder(){
        m_car = new BMWCar();
    }
    void BuildEngine(){
        m_car->SetEngine("BMW Engine.");
    }
    void BuildGearBox(){
        m_car->SetGearBox("BMW GearBox.");
    }
    void BuildChassis(){
        m_car->SetChassis("BMW Chassis.");
    }
    Car *BuildCar(){
        return m_car;
    }
private:
    Car *m_car;
};

//具体奔驰建造者类
class BenzCarBuilder: public CarBuilder
{
public:
    BenzCarBuilder(){
        m_car = new BenzCar();
    }
    void BuildEngine(){
        m_car->SetEngine("Benz Engine.");
    }
    void BuildGearBox(){
        m_car->SetGearBox("Benz GearBox.");
    }
    void BuildChassis(){
        m_car->SetChassis("Benz Chassis.");
    }
    Car *BuildCar(){
        return m_car;
    }
private:
    Car *m_car;
};

```
指导者（Director）
```cpp
//Director 指导者
class CarDirector
{
public:
    Car *ConstructCar(CarBuilder *carBuilder){
        carBuilder->BuildEngine();
        carBuilder->BuildGearBox();
        carBuilder->BuildChassis();
        printf("Car build finished.\n");
        return carBuilder->BuildCar();
    }
};

```
用户（Client）

```cpp
#include "carbuilder.h"

int main(int argc, char *argv[])
{
    //Client用户
    CarDirector *carDirector = new CarDirector();
    Car *bmwCar = carDirector->ConstructCar(new BMWCarBuilder());
    Car *benzCar = carDirector->ConstructCar(new BenzCarBuilder());

    delete bmwCar;
    delete benzCar;
    delete carDirector;
}

```
运行结果
![运行结果](运行结果.png)

补充说明

 　　本篇博客在完成过程中对于建造者模式有了不一样的理解，可以捕捉到一些建造者模式的精髓，该模式在创建ConcreteBuilder的时候其实与之前所写的工厂模式有一些相同之处，让我觉得有一丝闪闪发光的是Director类，通过这个类来指导一个简单的工厂模式按照一定流程完成一个产品的加工，Director就像是一个自动化产线，把各个零配件组装装配最终完成一个产品。

## Builder 模式
生活中有着很多的 Builder 的例子， 个人觉得大学生活就是一个 Builder 模式的最好体验：要完成大学教育， 一般将大学教育过程分成 4 个学期进行， 因此没有学习可以看作是构建完整大学教育的一个部分构建过程， 每个人经过这 4 年的（ 4 个阶段） 构建过程得到的最后的结果不一样， 因为可能在四个阶段的构建中引入了很多的参数（每个人的机会和际遇不完全相同）。Builder 模式要解决的也正是这样的问题：当我们要创建的对象很复杂的时候（通常是由很多其他的对象组合而成），我们要要复杂对象的创建过程和这个对象的表示（ 展示）分离开来， 这样做的好处就是通过一步步的进行复杂对象的构建， 由于在每一步的构造过程中可以引入参数，使得经过相同的步骤创建最后得到的对象的展示不一样。

Builder Pattern 结构图:
![Builder_Pattern_结构图](Builder_Pattern_结构图.png)

Builder 模式的关键是其中的 Director 对象并不直接返回对象，而是通过一步步（ BuildPartA， BuildPartB， BuildPartC）来一步步进行对象的创建。当然这里 Director 可以提供一个默认的返回对象的接口（即返回通用的复杂对象的创建， 即不指定或者特定唯一指定 BuildPart 中的参数）。
### 实现
```cpp
//Product.h
#ifndef _PRODUCT_H_
#define _PRODUCT_H_
class Product
{
public:
    Product();
    ~Product();
    void ProducePart();
protected:
private:
};
class ProductPart
{
public:
    ProductPart();
    ~ProductPart();
    ProductPart* BuildPart();
protected:
private:
};
#endif //~_PRODUCT_H_
```

```cpp
//Component.cpp
#include "Component.h"
Component::Component(){}
Component::~Component(){}
void Component::Add(const Component&com){}
Component* Component::GetChild(int index){return 0;}
void Component::Remove(const Component&com){}
```

```cpp
//Builder.h
#ifndef _BUILDER_H_
#define _BUILDER_H_
#include <string>
using namespace std;
class Product;
class Builder
{
public:
    virtual ~Builder();
    virtual void BuildPartA(const string& buildPara) = 0;
    virtual void BuildPartB(const string& buildPara) = 0;
    virtual void BuildPartC(const string& buildPara) = 0;
    virtual Product* GetProduct() = 0;
protected:
    Builder();
private:
};
class ConcreteBuilder:public Builder
{
public:
    ConcreteBuilder();
    ~ConcreteBuilder();
    void BuildPartA(const string&buildPara);
    void BuildPartB(const string& buildPara);
    void BuildPartC(const string& buildPara);
    Product* GetProduct();
protected:
private:
};
#endif //~_BUILDER_H_
```

```cpp
//Builder.cpp
#include "Builder.h"
#include "Product.h"
#include <iostream>
using namespace std;
Builder::Builder(){}
Builder::~Builder(){}
ConcreteBuilder::ConcreteBuilder(){}
ConcreteBuilder::~ConcreteBuilder(){}
void ConcreteBuilder::BuildPartA(const string& buildPara){
    cout<<"Step1:Build PartA..."<<buildPara<<endl;
}
void ConcreteBuilder::BuildPartB(const string& buildPara){
    cout<<"Step1:Build PartB..."<<buildPara<<endl;
}
void ConcreteBuilder::BuildPartC(const string& buildPara){
    cout<<"Step1:Build PartC..."<<buildPara<<endl;
}
Product* ConcreteBuilder::GetProduct(){
    BuildPartA("pre-defined");
    BuildPartB("pre-defined");
    BuildPartC("pre-defined");
    return new Product();
}
```

```cpp
//Director.h
#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_
class Builder;
class Director
{
public:
    Director(Builder* bld);
    ~Director();
    void Construct();
protected:
private:
    Builder* _bld;
};
#endif //~_DIRECTOR_H_
```

```cpp
//Director.cpp
#include "director.h"
#include "Builder.h"
Director::Director(Builder* bld){
    _bld = bld;
}
Director::~Director(){}
void Director::Construct(){
    _bld->BuildPartA("user-defined");
    _bld->BuildPartB("user-defined");
    _bld->BuildPartC("user-defined");
}
```

```cpp
//main.cpp
#include "Builder.h"
#include "Product.h"
#include "Director.h"
#include <iostream>
using namespace std;
int main(int argc,char* argv[])
{
    Director* d = new Director(new ConcreteBuilder());
    d->Construct();
    return 0;
}
```

Builder 模式的示例代码中， BuildPart 的参数是通过客户程序员传入的，这里为了简单说明问题，使用“ user-defined”代替，实际的可能是在 Construct 方法中传入这 3 个参数，这样就可以得到不同的细微差别的复杂对象了。

GoF 在《设计模式》一书中给出的关于 Builder 模式的意图是非常容易理解、间接的：将一个复杂对象的构建与它的表示分离， 使得同样的构建过程可以创建不同的表示（在示例代码中可以通过传入不同的参数实现这一点）。Builder 模式和 AbstractFactory 模式在功能上很相似，因为都是用来创建大的复杂的对象，它们的区别是： Builder 模式强调的是一步步创建对象，并通过相同的创建过程可以获得不同的结果对象，一般来说 Builder 模式中对象不是直接返回的。而在 AbstractFactory 模式中对象是直接返回的，bstractFactory 模式强调的是为创建多个相互依赖的对象提供一个同一的接口