---
noteId: "bdc894c0823811ea93dd41050c52b2f9"
tags: []

---

## 工厂方法模式

### 模式的定义与特点
工厂方法（FactoryMethod）模式的定义：定义一个创建产品对象的工厂接口，将产品对象的实际创建工作推迟到具体子工厂类当中。这满足创建型模式中所要求的“创建与使用相分离”的特点。

我们把被创建的对象称为“产品”，把创建产品的对象称为“工厂”。如果要创建的产品不多，只要一个工厂类就可以完成，这种模式叫“简单工厂模式”，它不属于 GoF 的 23 种经典设计模式，它的缺点是增加新产品时会违背“开闭原则”。

本节介绍的“工厂方法模式”是对简单工厂模式的进一步抽象化，其好处是可以使系统在不修改原来代码的情况下引进新的产品，即满足开闭原则。  
  
工厂方法模式的主要优点有：
    用户只需要知道具体工厂的名称就可得到所要的产品，无须知道产品的具体创建过程；
    在系统增加新的产品时只需要添加具体产品类和对应的具体工厂类，无须对原工厂进行任何修改，满足开闭原则

其缺点是：每增加一个产品就要增加一个具体产品类和一个对应的具体工厂类，这增加了系统的复杂度。

### 模式的结构与实现
工厂方法模式由抽象工厂、具体工厂、抽象产品和具体产品等4个要素构成。本节来分析其基本结构和实现方法

1. 模式的结构
工厂方法模式的主要角色如下。

 1. 抽象工厂（Abstract Factory）：提供了创建产品的接口，调用者通过它访问具体工厂的工厂方法 newProduct() 来创建产品。
 2. 具体工厂（ConcreteFactory）：主要是实现抽象工厂中的抽象方法，完成具体产品的创建。
 3. 抽象产品（Product）：定义了产品的规范，描述了产品的主要特性和功能。
 4. 具体产品（ConcreteProduct）：实现了抽象产品角色所定义的接口，由具体工厂来创建，它同具体工厂之间一一对应。

![工厂方法模式的结构图](工厂方法模式的结构图.gif)

2. 模式的实现  
根据图 1 写出该模式的代码如下:
```java
    package FactoryMethod;
    public class AbstractFactoryTest
    {
        public static void main(String[] args)
        {
            try
            {
                Product a;
                AbstractFactory af;
                af=(AbstractFactory) ReadXML1.getObject();
                a=af.newProduct();
                a.show();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    //抽象产品：提供了产品的接口
    interface Product
    {
        public void show();
    }
    //具体产品1：实现抽象产品中的抽象方法
    class ConcreteProduct1 implements Product
    {
        public void show()
        {
            System.out.println("具体产品1显示...");
        }
    }
    //具体产品2：实现抽象产品中的抽象方法
    class ConcreteProduct2 implements Product
    {
        public void show()
        {
            System.out.println("具体产品2显示...");
        }
    }
    //抽象工厂：提供了厂品的生成方法
    interface AbstractFactory
    {
        public Product newProduct();
    }
    //具体工厂1：实现了厂品的生成方法
    class ConcreteFactory1 implements AbstractFactory
    {
        public Product newProduct()
        {
            System.out.println("具体工厂1生成-->具体产品1...");
            return new ConcreteProduct1();
        }
    }
    //具体工厂2：实现了厂品的生成方法
    class ConcreteFactory2 implements AbstractFactory
    {
        public Product newProduct()
        {
            System.out.println("具体工厂2生成-->具体产品2...");
            return new ConcreteProduct2();
        }
    }
```

```java
    package FactoryMethod;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML1
    {
        //该方法用于从XML配置文件中提取具体类类名，并返回一个实例对象
        public static Object getObject()
        {
            try
            {
                //创建文档对象
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/FactoryMethod/config1.xml"));        
                //获取包含类名的文本节点
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=nl.item(0).getFirstChild();
                String cName="FactoryMethod."+classNode.getNodeValue();
                //System.out.println("新类名："+cName);
                //通过类名生成实例对象并将其返回
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
             }  
             catch(Exception e)
             {
                       e.printStackTrace();
                       return null;
             }
        }
    }
```
```
具体工厂1生成-->具体产品1...
具体产品1显示...
```
### 模式的应用实例

【例1】用工厂方法模式设计畜牧场。

分析：有很多种类的畜牧场，如养马场用于养马，养牛场用于养牛，所以该实例用工厂方法模式比较适合。

对养马场和养牛场等具体工厂类，只要定义一个生成动物的方法 newAnimal() 即可。由于要显示马类和牛类等具体产品类的图像，所以它们的构造函数中用到了 JPanel、JLabd 和 ImageIcon 等组件，并定义一个 show() 方法来显示它们。

客户端程序通过对象生成器类 ReadXML2 读取 XML 配置文件中的数据来决定养马还是养牛。其结构图如图 2 所示。
![畜牧场结构图](畜牧场结构图.gif)

```cpp
    package FactoryMethod;
    import java.awt.*;
    import javax.swing.*;
    public class AnimalFarmTest
    {
        public static void main(String[] args)
        {
            try
            {
                Animal a;
                AnimalFarm af;
                af=(AnimalFarm) ReadXML2.getObject();
                a=af.newAnimal();
                a.show();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    //抽象产品：动物类
    interface Animal
    {
        public void show();
    }
    //具体产品：马类
    class Horse implements Animal
    {
        JScrollPane sp;
        JFrame jf=new JFrame("工厂方法模式测试");
        public Horse()
        {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("动物：马"));
            sp=new JScrollPane(p1);
            contentPane.add(sp, BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/A_Horse.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //具体产品：牛类
    class Cattle implements Animal
    {
        JScrollPane sp;
        JFrame jf=new JFrame("工厂方法模式测试");
        public Cattle()
        {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("动物：牛"));
            sp=new JScrollPane(p1);
            contentPane.add(sp,BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/A_Cattle.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //抽象工厂：畜牧场
    interface AnimalFarm
    {
        public Animal newAnimal();
    }
    //具体工厂：养马场
    class HorseFarm implements AnimalFarm
    {
        public Animal newAnimal()
        {
            System.out.println("新马出生！");
            return new Horse();
        }
    }
    //具体工厂：养牛场
    class CattleFarm implements AnimalFarm
    {
        public Animal newAnimal()
        {
            System.out.println("新牛出生！");
            return new Cattle();
        }
    }
```

```java
    package FactoryMethod;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML2
    {
        public static Object getObject()
        {
            try
            {
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/FactoryMethod/config2.xml"));
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=nl.item(0).getFirstChild();
                String cName="FactoryMethod."+classNode.getNodeValue();
                System.out.println("新类名："+cName);
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
            }  
            catch(Exception e)
            {
                   e.printStackTrace();
                   return null;
            }
        }
    }
```
![畜牧场养殖的运行结果](畜牧场养殖的运行结果.gif)

### 模式的应用场景
工厂方法模式通常适用于以下场景。

    客户只知道创建产品的工厂名，而不知道具体的产品名。如 TCL 电视工厂、海信电视工厂等。
    创建对象的任务由多个具体子工厂中的某一个完成，而抽象工厂只提供创建产品的接口。
    客户不关心创建产品的细节，只关心产品的品牌。

### 模式的扩展
当需要生成的产品不多且不会增加，一个具体工厂类就可以完成任务时，可删除抽象工厂类。这时工厂方法模式将退化到简单工厂模式，其结构图如图 4 所示。
![简单工厂模式的结构图](简单工厂模式的结构图.gif)

## CPP

```cpp
//Prototype.h
#ifndef _PROTOTYPE_H_
#define _PROTOTYPE_H_
class Prototype
{
public:
    virtual ~Prototype();
    virtual Prototype* Clone() const = 0;
protected:
    Prototype();
private:
};
class ConcretePrototype:public Prototype
{
public:
    ConcretePrototype();
    ConcretePrototype(const
    ConcretePrototype& cp);
    ~ConcretePrototype();
    Prototype* Clone() const;
protected:
private:
};
#endif //~_PROTOTYPE_H_
```

```cpp
//Prototype.cpp
#include "Prototype.h"
#include <iostream>
using namespace std;
Prototype::Prototype(){
}
Prototype::~Prototype(){
}
Prototype* Prototype::Clone() const{
return 0;
}
ConcretePrototype::ConcretePrototype(){
}
ConcretePrototype::~ConcretePrototype(){
}
ConcretePrototype::ConcretePrototype(const ConcretePrototype& cp){
    cout<<"ConcretePrototype copy ..."<<endl;
}
Prototype* ConcretePrototype::Clone() const{
    return new ConcretePrototype(*this);
}
```

```cpp
//main.cpp
#include "Prototype.h"
#include <iostream>
using namespace std;
int main(int argc,char* argv[])
{
    Prototype* p = new ConcretePrototype();
    Prototype* p1 = p->Clone();
    return 0;
}
```

Prototype 模式的结构和实现都很简单，其关键就是（ C++中）拷贝构造函数的实现方
式， 这也是 C++实现技术层面上的事情。 由于在示例代码中不涉及到深层拷贝（主要指有指
针、复合对象的情况）， 因此我们通过编译器提供的默认的拷贝构造函数（按位拷贝）的方
式进行实现。 说明的是这一切只是为了实现简单起见， 也因为本文档的重点不在拷贝构造函
数的实现技术，而在 Prototype 模式本身的思想

Prototype 模式通过复制原型（ Prototype）而获得新对象创建的功能，这里 Prototype 本身就是“对象工厂”（ 因为能够生产对象） ， 实际上 Prototype 模式和 Builder 模式、AbstractFactory 模式都是通过一个类（对象实例） 来专门负责对象的创建工作（工厂对象），它们之间的区别是： Builder 模式重在复杂对象的一步步创建（并不直接返回对象） ，AbstractFactory 模式重在产生多个相互依赖类的对象，而 Prototype 模式重在从自身复制自己创建新类

