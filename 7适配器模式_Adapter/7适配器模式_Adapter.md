---
noteId: "5be1ab5083ca11eaa3219dc718be6a90"
tags: []

---

## 适配器模式（Adapter模式）详解

在现实生活中，经常出现两个对象因接口不兼容而不能在一起工作的实例，这时需要第三者进行适配。例如，讲中文的人同讲英文的人对话时需要一个翻译，用直流电的笔记本电脑接交流电源时需要一个电源适配器，用计算机访问照相机的 SD 内存卡时需要一个读卡器等。

在软件设计中也可能出现：需要开发的具有某种业务功能的组件在现有的组件库中已经存在，但它们与当前系统的接口规范不兼容，如果重新开发这些组件成本又很高，这时用适配器模式能很好地解决这些问题。

### 模式的定义与特点
适配器模式（Adapter）的定义如下：将一个类的接口转换成客户希望的另外一个接口，使得原本由于接口不兼容而不能一起工作的那些类能一起工作。适配器模式分为类结构型模式和对象结构型模式两种，前者类之间的耦合度比后者高，且要求程序员了解现有组件库中的相关组件的内部结构，所以应用相对较少些

该模式的主要优点如下。
- 客户端通过适配器可以透明地调用目标接口。
- 复用了现存的类，程序员不需要修改原有代码而重用现有的适配者类。
- 将目标类和适配者类解耦，解决了目标类和适配者类接口不一致的问题

### 模式的结构与实现
类适配器模式可采用多重继承方式实现，如 C++ 可定义一个适配器类来同时继承当前系统的业务接口和现有组件库中已经存在的组件接口；Java 不支持多继承，但可以定义一个适配器类来实现当前系统的业务接口，同时又继承现有组件库中已经存在的组件。

对象适配器模式可釆用将现有组件库中已经实现的组件引入适配器类中，该类同时实现当前系统的业务接口。现在来介绍它们的基本结构。
1. 模式的结构
适配器模式（Adapter）包含以下主要角色。
- 目标（Target）接口：当前系统业务所期待的接口，它可以是抽象类或接口。
- 适配者（Adaptee）类：它是被访问和适配的现存组件库中的组件接口。
- 适配器（Adapter）类：它是一个转换器，通过继承或引用适配者的对象，把适配者接口转换成目标接口，让客户按目标接口的格式访问适配者。
类适配器模式的结构图如图 1 所示。
![类适配器模式的结构图](类适配器模式的结构图.gif)

对象适配器模式的结构图如图 2 所示。
![对象适配器模式的结构图](对象适配器模式的结构图.gif)

2. 模式的实现
(1) 类适配器模式的代码如下。 
```java
    package adapter;
    //目标接口
    interface Target
    {
        public void request();
    }
    //适配者接口
    class Adaptee
    {
        public void specificRequest()
        {       
            System.out.println("适配者中的业务代码被调用！");
        }
    }
    //类适配器类
    class ClassAdapter extends Adaptee implements Target
    {
        public void request()
        {
            specificRequest();
        }
    }
    //客户端代码
    public class ClassAdapterTest
    {
        public static void main(String[] args)
        {
            System.out.println("类适配器模式测试：");
            Target target = new ClassAdapter();
            target.request();
        }
    }
```
```
类适配器模式测试：
适配者中的业务代码被调用！
```
(2)对象适配器模式的代码如下。
```cpp
    package adapter;
    //对象适配器类
    class ObjectAdapter implements Target
    {
        private Adaptee adaptee;
        public ObjectAdapter(Adaptee adaptee)        {
            this.adaptee=adaptee;
        }
        public void request()        {
            adaptee.specificRequest();
        }
    }
    //客户端代码
    public class ObjectAdapterTest
    {
        public static void main(String[] args) {
            System.out.println("对象适配器模式测试：");
            Adaptee adaptee = new Adaptee();
            Target target = new ObjectAdapter(adaptee);
            target.request();
        }
    }
```
说明：对象适配器模式中的“目标接口”和“适配者类”的代码同类适配器模式一样，只要修改适配器类和客户端的代码即可。
```
对象适配器模式测试：
适配者中的业务代码被调用！
```

### 模式的应用实例
【例1】用适配器模式（Adapter）模拟新能源汽车的发动机。
分析：新能源汽车的发动机有电能发动机（Electric Motor）和光能发动机（Optical Motor）等，各种发动机的驱动方法不同，例如，电能发动机的驱动方法 electricDrive() 是用电能驱动，而光能发动机的驱动方法 opticalDrive() 是用光能驱动，它们是适配器模式中被访问的适配者。

客户端希望用统一的发动机驱动方法 drive() 访问这两种发动机，所以必须定义一个统一的目标接口 Motor，然后再定义电能适配器（Electric Adapter）和光能适配器（Optical Adapter）去适配这两种发动机。

我们把客户端想访问的新能源发动机的适配器的名称放在 XML 配置文件中（点此下载 XML 文件），客户端可以通过对象生成器类 ReadXML 去读取。这样，客户端就可以通过 Motor 接口随便使用任意一种新能源发动机去驱动汽车，图 3 所示是其结构图。
![发动机适配器的结构图](发动机适配器的结构图.gif)

```java
    package adapter;
    //目标：发动机
    interface Motor {
        public void drive();
    }
    //适配者1：电能发动机
    class ElectricMotor {
        public void electricDrive() {
            System.out.println("电能发动机驱动汽车！");
        }
    }
    //适配者2：光能发动机
    class OpticalMotor
    {
        public void opticalDrive() {
            System.out.println("光能发动机驱动汽车！");
        }
    }
    //电能适配器
    class ElectricAdapter implements Motor
    {
        private ElectricMotor emotor;
        public ElectricAdapter(){
            emotor=new ElectricMotor();
        }
        public void drive(){
            emotor.electricDrive();
        }
    }
    //光能适配器
    class OpticalAdapter implements Motor
    {
        private OpticalMotor omotor;
        public OpticalAdapter(){
            omotor=new OpticalMotor();
        }
        public void drive(){
            omotor.opticalDrive();
        }
    }
    //客户端代码
    public class MotorAdapterTest
    {
        public static void main(String[] args)
        {
            System.out.println("适配器模式测试：");
            Motor motor=(Motor)ReadXML.getObject();
            motor.drive();
        }
    }
```

```java
    package adapter;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML
    {
        public static Object getObject()
        {
            try
            {
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/adapter/config.xml"));
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=nl.item(0).getFirstChild();
                String cName="adapter."+classNode.getNodeValue();
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
             }  
             catch(Exception e)
             {
                       e.printStackTrace();
                       return null;
             }
        }
    }
```
```
适配器模式测试：
电能发动机驱动汽车！
```
```
注意：如果将配置文件中的 ElectricAdapter 改为 OpticalAdapter，则运行结果如下：

适配器模式测试：
光能发动机驱动汽车！
```
### 模式的应用场景
适配器模式（Adapter）通常适用于以下场景。
- 以前开发的系统存在满足新系统功能需求的类，但其接口同新系统的接口不一致。
- 使用第三方提供的组件，但组件接口定义和自己要求的接口定义不同。

### 模式的扩展
适配器模式（Adapter）可扩展为双向适配器模式，双向适配器类既可以把适配者接口转换成目标接口，也可以把目标接口转换成适配者接口，其结构图如图 4 所示。
![双向适配器模式的结构图](双向适配器模式的结构图.gif)

```java
    package adapter;
    //目标接口
    interface TwoWayTarget{
        public void request();
    }
    //适配者接口
    interface TwoWayAdaptee{
        public void specificRequest();
    }
    //目标实现
    class TargetRealize implements TwoWayTarget{
        public void request(){       
            System.out.println("目标代码被调用！");
        }
    }
    //适配者实现
    class AdapteeRealize implements TwoWayAdaptee{
        public void specificRequest(){       
            System.out.println("适配者代码被调用！");
        }
    }
    //双向适配器
    class TwoWayAdapter  implements TwoWayTarget,TwoWayAdaptee
    {
        private TwoWayTarget target;
        private TwoWayAdaptee adaptee;
        public TwoWayAdapter(TwoWayTarget target){
            this.target=target;
        }
        public TwoWayAdapter(TwoWayAdaptee adaptee){
            this.adaptee=adaptee;
        }
        public void request(){
            adaptee.specificRequest();
        }
        public void specificRequest(){       
            target.request();
        }
    }
    //客户端代码
    public class TwoWayAdapterTest
    {
        public static void main(String[] args)
        {
            System.out.println("目标通过双向适配器访问适配者：");
            TwoWayAdaptee adaptee=new AdapteeRealize();
            TwoWayTarget target=new TwoWayAdapter(adaptee);
            target.request();
            System.out.println("-------------------");
            System.out.println("适配者通过双向适配器访问目标：");
            target=new TargetRealize();
            adaptee=new TwoWayAdapter(target);
            adaptee.specificRequest();
        }
    }
```
```
目标通过双向适配器访问适配者：
适配者代码被调用！
-------------------
适配者通过双向适配器访问目标：
目标代码被调用！
```

## CPP
本章主要说明适配器模式，该设计模式主要意图是：将一个类的接口转换成当前需求另一个接口的格式，适配器模式使原本由于接口不兼容无法工作的那些类可以一起工作。
适配器模式做细分有两种：
- 面向类的适配器模式
- 面向对象的适配器模式
### 面向类的适配器
这里我们举一个面向类的适配器，面向类的适配器通过继承转换调用函数接口的方式进行类的适配，在这个例子中，已存在Deque类（类似双向列表），需要适配的接口Stack类（类似栈），通过一个Adapter类实现使用stack的函数接口，但调用Deque的函数实现。
### 主要参与者
该设计模式的参与者有4个，分别是：
- Target 定义Client使用的特定接口
- Client 需要适配的
- Adaptee 一个已经存在的接口
- Adapter 对Adaptee与Target接口进行适配的适配器 deque

### 具体实现代码

现有接口（Adaptee ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef DEQUE_H
#define DEQUE_H
#include <vector>
#include <stdio.h>
//Adaptee 已经存在的接口
class Deque
{
public:
    Deque(){}
    void pop_back() {
        if(m_dnum.size()<=0) {
            return;
        }
        else{
            m_dnum.erase(m_dnum.end() -1);
            printf("this is class Deque popback\n");
        }
    }
    void pop_front() {
        if(m_dnum.size()<=0) {
            return;
        }
        else {
            m_dnum.erase(m_dnum.begin());
            printf("this is class Deque popfront\n");
        }
    }
    void push_back(int num) {
        m_dnum.push_back(num);
        printf("this is class Deque pushback\n");
    }
    void push_front(int num) {
        m_dnum.insert(m_dnum.begin(), num);
        printf("this is class Deque pushfront\n");
    }
private:
    std::vector<int> m_dnum;
};
#endif // DEQUE_H

```

对接接口（Target ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef STACK_H
#define STACK_H
#include <vector>
//Target 对接接口
class Stack
{
public:
    Stack(){}
    virtual void pop() {
        if(m_num.size()<=0)
            return;
        else
            m_num.erase(m_num.end());
    }
    virtual void push(int num) {
        m_num.push_back(num);
    }
private:
    std::vector<int> m_num;
};
#endif // STACK_H
```

类适配器（Adapter ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef ADAPTER_H
#define ADAPTER_H
#include "Deque.h"
#include "Stack.h"
//Adapter 适配器
class Adapter :public Deque, public Stack
{
public:
    Adapter(){}
    void push(int num)
    {
        this->push_back(num);
    }
    void pop()
    {
        this->pop_back();
    }
};
#endif // ADAPTER_H

```
用户使用（Client）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#include "Adapter.h"
// Client 需要适配的
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Stack *s = new Adapter();
    s->push(0);
    s->push(1);
    s->pop();
    s->pop();
    return a.exec();
}

```
![输出结果](输出结果.png)


### 面向对象的适配器
这里我们举一个面向对象的适配器，面向对象的适配器通过包含适配器对象直接进行适配，在这个例子中，已存在Deque类（Adapter 类似双向列表），特定接口Suquence( Adaptee)，继承接口并包含Deque对象的Target类。
Stack和Queue通过操作Deque来实现接口适配。
#### 主要参与者
- Target 定义Client使用的特定接口
- Client 需要适配的
- Adaptee 一个已经存在的接口
- Adapter 对Adaptee与Target接口进行适配的适配器 deque

### 具体实现代码
类适配器（Adapter ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef DEQUE_H
#define DEQUE_H
#include <vector>
#include <stdio.h>

class Deque
{
public:
    Deque(){}
    void pop_back()
    {
        if(m_dnum.size()<=0)
        {
            return;
        }
        else
        {
            printf(" class Deque popback %d\n", m_dnum.back());
            m_dnum.erase(m_dnum.end() -1);
        }
    }
    void pop_front()
    {
        if(m_dnum.size()<=0)
        {
            return;
        }
        else
        {
            printf(" class Deque popfront %d\n", m_dnum.front());
            m_dnum.erase(m_dnum.begin());
        }
    }
    void push_back(int num)
    {
        printf(" class Deque pushback %d\n", num);
        m_dnum.push_back(num);
    }
    void push_front(int num)
    {
        m_dnum.insert(m_dnum.begin(), num);
        printf(" class Deque pushfront %d\n", num);
    }
private:
    std::vector<int> m_dnum;
};
#endif // DEQUE_H

```

现有接口（Adaptee ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef SEQUENCE_H
#define SEQUENCE_H

class Sequence
{
public:
    Sequence(){}
    virtual void push(int num) = 0;
    virtual void pop() = 0;
};
#endif // SEQUENCE_H

```

对接接口（Target ）
```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef QUEUE_H
#define QUEUE_H
#include "Deque.h"
#include "Sequence.h"
class Queue : public Sequence
{
public:
    Queue(){m_num = new Deque;}
    void pop()
    {
        printf("class Queue,");
        m_num->pop_front();
    }
    void push(int num)
    {
        printf("class Queue,");
        m_num->push_back(num);
    }
private:
    Deque *m_num;
};
#endif // QUEUE_H

```

```cpp
/****************************************************************
 Author :   BingLee 
 Date   :   2019-07-16
 Info   :   
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#ifndef STACK_H
#define STACK_H

#include "Sequence.h"
#include "Deque.h"

class Stack : public Sequence
{
public:
    Stack(){m_num = new Deque;}
    void pop()
    {
        printf("class Stack,");
        m_num->pop_back();
    }
    void push(int num)
    {
        printf("class Stack,");
        m_num->push_back(num);
    }
private:
    Deque *m_num;
};
#endif // STACK_H
```

用户使用（Client）
```cpp
#include <QCoreApplication>
#include "Stack.h"
#include "Queue.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Sequence *stack = new Stack();
    Sequence *queue = new Queue();
    stack->push(0);
    stack->push(1);
    queue->push(0);
    queue->push(1);
    stack->pop();	//stack pop from back of vector
    queue->pop();	//queue pop from front of vector
    delete stack,queue;
    return a.exec();
}

```
![输出结果](输出结果.png)
这里可以发现，stack调用pop从末端弹出数据，queue调用pop从前端弹出数据

### 补充说明
整篇文章讲了类适配器与对象适配器的实现，那在什么情况下我们可以使用适配器模式呢？
- 在日常开发中可能会遇到工具箱中的一个类所提供的函数接口与我们的需求不同；
- 想创建一个可以复用的类，该类可以与其他不相关的类或未来可能遇到的类协同工作；
- 像对象适配器中所示使用一些已经存在的类，对他们的父类接口进行适配


--- 

Adapter 模式解决的问题在生活中经常会遇到：比如我们有一个 Team 为外界提供 S 类服务，但是我们 Team 里面没有能够完成此项人物的 member，然后我们得知有 A 可以完成这项服务（他把这项人物重新取了个名字叫 S’， 并且他不对外公布他的具体实现）。 为了保证我们对外的服务类别的一致性（提供 S 服务），我们有以下两种方式解决这个问题：

1）把 B 君直接招安到我们 Team 为我们工作，提供 S 服务的时候让 B 君去办就是了；
2） B 君可能在别的地方有工作，并且不准备接受我们的招安，于是我们 Team 可以想这样一种方式解决问题：我们安排 C 君去完成这项任务，并做好工作（ Money： ））让 A 君工作的时候可以向 B 君请教，因此 C 君就是一个复合体（提供 S 服务，但是是 B 君的继承弟子）。

实际上在软件系统设计和开发中， 这种问题也会经常遇到： 我们为了完成某项工作购买了一个第三方的库来加快开发。 这就带来了一个问题： 我们在应用程序中已经设计好了接口，与这个第三方提供的接口不一致， 为了使得这些接口不兼容的类（不能在一起工作） 可以在一起工作了， Adapter 模式提供了将一个类（第三方库）的接口转化为客户（购买使用者）希望的接口。在上面生活中问题的解决方式也就正好对应了 Adapter 模式的两种类别： 类模式和对象模式。
### 模式选择
```cpp
//Adapter.h
#ifndef _ADAPTER_H_
#define _ADAPTER_H_
class Target
{
public:
    Target();
    virtual ~Target();
    virtual void Request();
protected:
private:
};
class Adaptee
{
public:
    Adaptee();
    ~Adaptee();
    void SpecificRequest();
protected:
private:
};
class Adapter:public Target,private Adaptee
{
public:
    Adapter();
    ~Adapter();
    void Request();
protected:
private:
};
#endif //~_ADAPTER_H_
```

```cpp
//Adapter.cpp
#include "Adapter.h"
#include <iostream>
Target::Target(){}
Target::~Target(){}
void Target::Request(){
std::cout<<"Target::Request"<<std::endl;
}
Adaptee::Adaptee(){}
Adaptee::~Adaptee(){}
    void Adaptee::SpecificRequest(){
    std::cout<<"Adaptee::SpecificRequest"<<std::endl;
}
Adapter::Adapter(){}
Adapter::~Adapter(){}
    void Adapter::Request(){
        this->SpecificRequest();
    }
```

```cpp
//Adapter.h
#ifndef _ADAPTER_H_
#define _ADAPTER_H_
class Target
{
public:
    Target();
    virtual ~Target();
    virtual void Request();
protected:
private:
};
class Adaptee
{
public:
    Adaptee();
    ~Adaptee();
    void SpecificRequest();
protected:
private:
};
class Adapter:public Target
{
public:
    Adapter(Adaptee* ade);
    ~Adapter();
    void Request();
protected:
private:
Adaptee* _ade;
};
#endif //~_ADAPTER_H_
```

```cpp
//Adapter.cpp
#include "Adapter.h"
#include <iostream>
Target::Target(){}
Target::~Target(){}
void Target::Request(){
    std::cout<<"Target::Request"<<std::endl;    
}
Adaptee::Adaptee(){}
Adaptee::~Adaptee(){}
void Adaptee::SpecificRequest(){
std::cout<<"Adaptee::SpecificRequest"<<std::endl;
}
Adapter::Adapter(Adaptee* ade){
    this->_ade = ade;
}
Adapter::~Adapter(){}
void Adapter::Request(){
    _ade->SpecificRequest();
}
```

```cpp
//main.cpp
#include "Adapter.h"
#include <iostream>
using namespace std;
int main(int argc,char* argv[])
{
    Adaptee* ade = new Adaptee;
    Target* adt = new Adapter(ade);
    adt->Request();
    return 0;
}
```

### 代码说明
Adapter 模式实现上比较简单，要说明的是在类模式 Adapter 中，我们通过 private 继承Adaptee 获得实现继承的效果， 而通过 public 继承 Target 获得接口继承的效果（有关实现继承和接口继承参见讨论部分）。
  
在 Adapter 模式的两种模式中， 有一个很重要的概念就是接口继承和实现继承的区别和联系。接口继承和实现继承是面向对象领域的两个重要的概念，接口继承指的是通过继承，子类获得了父类的接口，而实现继承指的是通过继承子类获得了父类的实现（并不统共接口）。在 C++中的 public 继承既是接口继承又是实现继承，因为子类在继承了父类后既可以对外提供父类中的接口操作， 又可以获得父类的接口实现。 当然我们可以通过一定的方式和技术模拟单独的接口继承和实现继承，例如我们可以通过 private 继承获得实现继承的效果（ private 继承后，父类中的接口都变为 private，当然只能是实现继承了。 ），通过纯抽象基类模拟接口继承的效果， 但是在 C++中 pure virtual function 也可以提供默认实现， 因此这是不纯正的接口继承，但是在 Java 中我们可以 interface 来获得真正的接口继承了。
