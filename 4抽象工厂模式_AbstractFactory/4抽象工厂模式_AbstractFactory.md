---
noteId: "5924ce9082b411eaafa3834653100b8b"
tags: []

---

## 抽象工厂模式（详解版）

前面介绍的工厂方法模式中考虑的是一类产品的生产，如畜牧场只养动物、电视机厂只生产电视机、计算机软件学院只培养计算机软件专业的学生等。  
同种类称为同等级，也就是说：工厂方法模式只考虑生产同等级的产品，但是在现实生活中许多工厂是综合型的工厂，能生产多等级（种类） 的产品，如农场里既养动物又种植物，电器厂既生产电视机又生产洗衣机或空调，大学既有软件专业又有生物专业等。  
本节要介绍的抽象工厂模式将考虑多等级产品的生产，将同一个具体工厂所生产的位于不同等级的一组产品称为一个产品族，图 1 所示的是海尔工厂和 TCL 工厂所生产的电视机与空调对应的关系图。
![电器工厂的产品等级与产品族](电器工厂的产品等级与产品族.gif)

### 模式的定义与特点
抽象工厂（AbstractFactory）模式的定义：是一种为访问类提供一个创建一组相关或相互依赖对象的接口，且访问类无须指定所要产品的具体类就能得到同族的不同等级的产品的模式结构。
抽象工厂模式是工厂方法模式的升级版本，工厂方法模式只生产一个等级的产品，而抽象工厂模式可生产多个等级的产品。

使用抽象工厂模式一般要满足以下条件。
    系统中有多个产品族，每个具体工厂创建同一族但属于不同等级结构的产品。
    系统一次只可能消费其中某一族产品，即同族的产品一起使用。

抽象工厂模式除了具有工厂方法模式的优点外，其他主要优点如下。
    可以在类的内部对产品族中相关联的多等级产品共同管理，而不必专门引入多个新的类来进行管理。
    当增加一个新的产品族时不需要修改原代码，满足开闭原则。

其缺点是：当产品族中需要增加一个新的产品时，所有的工厂类都需要进行修改。

### 模式的结构与实现
抽象工厂模式同工厂方法模式一样，也是由抽象工厂、具体工厂、抽象产品和具体产品等 4 个要素构成，但抽象工厂中方法个数不同，抽象产品的个数也不同。现在我们来分析其基本结构和实现方法。
1. 模式的结构
抽象工厂模式的主要角色如下。
>1.  抽象工厂（Abstract Factory）：提供了创建产品的接口，它包含多个创建产品的方法 newProduct()，可以创建多个不同等级的产品。
> 2. 具体工厂（Concrete Factory）：主要是实现抽象工厂中的多个抽象方法，完成具体产品的创建。
> 3. 抽象产品（Product）：定义了产品的规范，描述了产品的主要特性和功能，抽象工厂模式有多个抽象产品。
> 4. 具体产品（ConcreteProduct）：实现了抽象产品角色所定义的接口，由具体工厂来创建，它 同具体工厂之间是多对一的关系。

抽象工厂模式的结构图如图 所示
![抽象工厂模式的结构图](抽象工厂模式的结构图.gif)

2. 模式的实现
从图抽象工厂模式的结构图可以看出抽象工厂模式的结构同工厂方法模式的结构相似，不同的是其产品的种类不止一个，所以创建产品的方法也不止一个。下面给出抽象工厂和具体工厂的代码。

(1) 抽象工厂：提供了产品的生成方法。
```java
    interface AbstractFactory
    {
        public Product1 newProduct1();
        public Product2 newProduct2();
    }
```
(2) 具体工厂：实现了产品的生成方法。
```java
    class ConcreteFactory1 implements AbstractFactory
    {
        public Product1 newProduct1()
        {
            System.out.println("具体工厂 1 生成-->具体产品 11...");
            return new ConcreteProduct11();
        }
        public Product2 newProduct2()
        {
            System.out.println("具体工厂 1 生成-->具体产品 21...");
            return new ConcreteProduct21();
        }
    }
```

### 模式的应用实例
【例1】用抽象工厂模式设计农场类。
分析：农场中除了像畜牧场一样可以养动物，还可以培养植物，如养马、养牛、种菜、种水果等，所以本实例比前面介绍的畜牧场类复杂，必须用抽象工厂模式来实现。

本例用抽象工厂模式来设计两个农场，一个是韶关农场用于养牛和种菜，一个是上饶农场用于养马和种水果，可以在以上两个农场中定义一个生成动物的方法 newAnimal() 和一个培养植物的方法 newPlant()。

对马类、牛类、蔬菜类和水果类等具体产品类，由于要显示它们的图像，所以它们的构造函数中用到了 JPanel、JLabel 和 ImageIcon 等组件，并定义一个 show() 方法来显示它们。

客户端程序通过对象生成器类 ReadXML 读取 XML 配置文件中的数据来决定养什么动物和培养什么植物。其结构图如图 3 所示
![农场类的结构图](农场类的结构图.gif)

```java
    package AbstractFactory;
    import java.awt.*;
    import javax.swing.*;
    public class FarmTest
    {
        public static void main(String[] args)
        {
            try
            {          
                Farm f;
                Animal a;
                Plant p;
                f=(Farm) ReadXML.getObject();
                a=f.newAnimal();
                p=f.newPlant();
                a.show();
                p.show();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    //抽象产品：动物类
    interface Animal
    {
        public void show();
    }
    //具体产品：马类
    class Horse implements Animal
    {
        JScrollPane sp;
        JFrame jf=new JFrame("抽象工厂模式测试");
        public Horse()
        {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("动物：马"));
            sp=new JScrollPane(p1);
            contentPane.add(sp, BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/A_Horse.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //具体产品：牛类
    class Cattle implements Animal
    {
        JScrollPane sp;
        JFrame jf=new JFrame("抽象工厂模式测试");
        public Cattle() {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("动物：牛"));
            sp=new JScrollPane(p1);
            contentPane.add(sp, BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/A_Cattle.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //抽象产品：植物类
    interface Plant
    {
        public void show();
    }
    //具体产品：水果类
    class Fruitage implements Plant
    {
        JScrollPane sp;
        JFrame jf=new JFrame("抽象工厂模式测试");
        public Fruitage()
        {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("植物：水果"));
            sp=new JScrollPane(p1);
            contentPane.add(sp, BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/P_Fruitage.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //具体产品：蔬菜类
    class Vegetables implements Plant
    {
        JScrollPane sp;
        JFrame jf=new JFrame("抽象工厂模式测试");
        public Vegetables()
        {
            Container contentPane=jf.getContentPane();
            JPanel p1=new JPanel();
            p1.setLayout(new GridLayout(1,1));
            p1.setBorder(BorderFactory.createTitledBorder("植物：蔬菜"));
            sp=new JScrollPane(p1);
            contentPane.add(sp, BorderLayout.CENTER);
            JLabel l1=new JLabel(new ImageIcon("src/P_Vegetables.jpg"));
            p1.add(l1);       
            jf.pack();       
            jf.setVisible(false);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户点击窗口关闭 
        }
        public void show()
        {
            jf.setVisible(true);
        }
    }
    //抽象工厂：农场类
    interface Farm
    {
        public Animal newAnimal();
        public Plant newPlant();
    }
    //具体工厂：韶关农场类
    class SGfarm implements Farm
    {
        public Animal newAnimal()
        {
            System.out.println("新牛出生！");
            return new Cattle();
        }
        public Plant newPlant()
        {
            System.out.println("蔬菜长成！");
            return new Vegetables();
        }
    }
    //具体工厂：上饶农场类
    class SRfarm implements Farm
    {
        public Animal newAnimal()
        {
            System.out.println("新马出生！");
            return new Horse();
        }
        public Plant newPlant()
        {
            System.out.println("水果长成！");
            return new Fruitage();
        }
    }
```
```java
    package AbstractFactory;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML
    {
        public static Object getObject()
        {
            try
            {
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/AbstractFactory/config.xml"));
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=nl.item(0).getFirstChild();
                String cName="AbstractFactory."+classNode.getNodeValue();
                System.out.println("新类名："+cName);
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
            }  
            catch(Exception e)
            {
                   e.printStackTrace();
                   return null;
            }
        }
    }
```
程序运行结果如图 
![农场养殖的运行结果](农场养殖的运行结果.jpg)

### 模式的应用场景
抽象工厂模式最早的应用是用于创建属于不同操作系统的视窗构件。如 java 的 AWT 中的 Button 和 Text 等构件在 Windows 和 UNIX 中的本地实现是不同的。

抽象工厂模式通常适用于以下场景：

> 当需要创建的对象是一系列相互关联或相互依赖的产品族时，如电器工厂中的电视机、洗衣机、空调等。
> 系统中有多个产品族，但每次只使用其中的某一族产品。如有人只喜欢穿某一个品牌的衣服和鞋。
> 系统中提供了产品的类库，且所有产品的接口相同，客户端不依赖产品实例的创建细节和内部结构。

### 模式的扩展
抽象工厂模式的扩展有一定的“开闭原则”倾斜性：

    当增加一个新的产品族时只需增加一个新的具体工厂，不需要修改原代码，满足开闭原则。
    当产品族中需要增加一个新种类的产品时，则所有的工厂类都需要进行修改，不满足开闭原则。


另一方面，当系统中只存在一个等级结构的产品时，抽象工厂模式将退化到工厂方法模式。

## CPP
### AbstactFactory 模式
> 问题
假设我们要开发一款游戏， 当然为了吸引更多的人玩， 游戏难度不能太大（让大家都没
有信心了， 估计游戏也就没有前途了），但是也不能太简单（没有挑战性也不符合玩家的心
理）。于是我们就可以采用这样一种处理策略： 为游戏设立等级，初级、中级、高级甚至有
BT 级。 假设也是过关的游戏， 每个关卡都有一些怪物（ monster） 守着， 玩家要把这些怪物
干掉才可以过关。 作为开发者， 我们就不得不创建怪物的类， 然后初级怪物、 中级怪物等都
继承自怪物类（当然不同种类的则需要另创建类，但是模式相同）。在每个关卡， 我们都要
创建怪物的实例，例如初级就创建初级怪物（ 有很多种类）、中级创建中级怪物等。可以想
象在这个系统中， 将会有成千上万的怪物实例要创建， 问题是还要保证创建的时候不会出错：
初级不能创建 BT 级的怪物（玩家就郁闷了，玩家一郁闷，游戏也就挂挂了），反之也不可
以。
AbstractFactory 模式就是用来解决这类问题的：要创建一组相关或者相互依赖的对象

### 模式选择
AbstractFactory 模式典型的结构图为：
![AbstractFactory_Pattern_结构图](AbstractFactory_Pattern_结构图.png)
AbstractFactory 模式关键就是将这一组对象的创建封装到一个用于创建对象的类
（ ConcreteFactory） 中， 维护这样一个创建类总比维护 n 多相关对象的创建过程要简单的多。

### 实现
完整代码示例（ code）
```cpp
//Product.h
#ifndef _PRODUCT_H_
#define _PRODUCT_H_
class AbstractProductA
{
public:
    virtual ~AbstractProductA();
protected:
    AbstractProductA();
private:
};
class AbstractProductB
{
public:
    virtual ~AbstractProductB();
protected:
    AbstractProductB(); private:
};
class ProductA1:public AbstractProductA
{
public:
    ProductA1();
    ~ProductA1();
protected: private:
};
class ProductA2:public AbstractProductA
{
public:
    ProductA2();
    ~ProductA2();
protected: private:
};
class ProductB1:public AbstractProductB
{
public:
    ProductB1();
    ~ProductB1();
protected:
private:
};
class ProductB2:public AbstractProductB
{
public:
    ProductB2();
    ~ProductB2();
protected: private:
};
#endif //~_PRODUCT_H_ECT_H_
```

```cpp
//Product.cpp
#include "Product.h"
#include <iostream>
using namespace std;
AbstractProductA::AbstractProductA(){ }
AbstractProductA::~AbstractProductA(){ }
AbstractProductB::AbstractProductB(){}
AbstractProductB::~AbstractProductB(){}
ProductA1::ProductA1(){
    cout<<"ProductA1..."<<endl;
}
ProductA1::~ProductA1(){}
ProductA2::ProductA2(){
    cout<<"ProductA2..."<<endl;
}
ProductA2::~ProductA2(){}
ProductB1::ProductB1(){
    cout<<"ProductB1..."<<endl;
}
ProductB1::~ProductB1(){
}
ProductB2::ProductB2(){
cout<<"ProductB2..."<<endl;
}
ProductB2::~ProductB2(){}
```

```cpp
//AbstractFactory.h
#ifndef _ABSTRACTFACTORY_H_
#define _ABSTRACTFACTORY_H_
class AbstractProductA;
class AbstractProductB;
class AbstractFactory
{
public:
    virtual ~AbstractFactory();
    virtual AbstractProductA*
    CreateProductA() = 0;
    virtual AbstractProductB*
    CreateProductB() = 0;
protected:
    AbstractFactory();
private:
};
class ConcreteFactory1:public AbstractFactory
{
public:
    ConcreteFactory1();
    ~ConcreteFactory1();
    AbstractProductA* CreateProductA();
    AbstractProductB* CreateProductB();
protected:
private:
};
class ConcreteFactory2:public AbstractFactory
{
public:
    ConcreteFactory2();
    ~ConcreteFactory2();
    AbstractProductA* CreateProductA();
    AbstractProductB* CreateProductB();
protected:
private:
};
#endif //~_ABSTRACTFACTORY_H_
```

```cpp
//AbstractFactory.cpp
#include "AbstractFactory.h"
#include "Product.h"
#include <iostream>
using namespace std;
AbstractFactory::AbstractFactory(){}
AbstractFactory::~AbstractFactory(){}
ConcreteFactory1::ConcreteFactory1(){}
ConcreteFactory1::~ConcreteFactory1(){}
AbstractProductA*ConcreteFactory1::CreateProductA(){
    return new ProductA1();}
AbstractProductB*ConcreteFactory1::CreateProductB(){
    return new ProductB1();
}
ConcreteFactory2::ConcreteFactory2(){}
ConcreteFactory2::~ConcreteFactory2(){}
AbstractProductA*ConcreteFactory2::CreateProductA(){
    return new ProductA2();
}
AbstractProductB* ConcreteFactory2::CreateProductB(){
    return new ProductB2();
}
```
```cpp
//main.cpp
#include "AbstractFactory.h"
#include <iostream>
using namespace std;
int main(int argc,char* argv[])
{
    AbstractFactory* cf1 = new ConcreteFactory1();
    cf1->CreateProductA();
    cf1->CreateProductB();
    AbstractFactory* cf2 = new ConcreteFactory2();
    cf2->CreateProductA();
    cf2->CreateProductB();
return 0;
}
```
AbstractFactory 模式的实现代码很简单， 在测试程序中可以看到， 当我们要创建一组对象（ ProductA1， ProductA2）的时候我们只用维护一个创建对象（ConcreteFactory1），大大简化了维护的成本和工作

AbstractFactory 模式和 Factory 模式的区别是初学（使用）设计模式时候的一个容易引起困惑的地方。 实际上， AbstractFactory 模式是为创建一组（ 有多类） 相关或依赖的对象提
供创建接口， 而 Factory 模式正如我在相应的文档中分析的是为一类对象提供创建接口或延迟对象的创建到子类中实现。并且可以看到， AbstractFactory 模式通常都是使用 Factory 模式实现（ ConcreteFactory1）。


## 抽象工厂模式
在做面向对象的软件开发时我们往往想达到更高的代码可复用性和更合理的软件颗粒度。
　　根据《设计模式——可复用面向对象软件的基础》所说：“你必须找到相关的对象，以适当的颗粒度将他们回归类，再定义类的接口和继承层次，建立对象之间的基本关系。你的设计应该对手头的问题有针对性，同时对将来的问题和需求也要有足够的通用性。”
　　内行的设计者知道：不是解决任何问题都要从头做起。他们更愿意复用以前使用的解决方案。这些重复的模式方案解决特定的问题，使得面向对象的设计更灵活、优雅，最终复用性更好。它们帮助设计者将新的设计建立在以往的工作基础上，复用以往的成功设计方案。一个熟悉这些设计模式的设计者不需要再去发现它们，而能够立即将他们应用于设计问题中。
　　本系列文章主要参考文献为——设计模式，可复用面向对象软件的基础（Design Patterns Elements of Reusable Object-Oriented SoftWare Erich.），内部代码基本用C++语言编写。
[23种设计模式C++实现——概要（索引汇总](https://blog.csdn.net/Bing_Lee/article/details/87640606)

### 摘要
本章主要说明抽象工厂设计模式，该设计模式相对于简单工厂模式，工厂方法模式更为灵活。工厂方法模式和抽象工厂模式都包含抽象产品和抽象工厂，抽象工厂模式的区别在于，其抽象工厂对于对个抽象产品打包生产的特性，去掉该特性即是工厂方法模式。同样的，工厂方法模式去掉抽象工厂类转为实体类则是简单工厂模式，对于模式的学习可以通过比对的方式让自己对于类似相近模式有更深的理解。

### 具体实现代码
```cpp
#include <stdio.h>
#include <string>
//抽象引擎配件类
class Engine
{
public:
    virtual void CreatEngine() = 0;
};
//具体引擎配件类
class BMW320Engine : public Engine
{
public:
    BMW320Engine(){this->CreatEngine();}
    void CreatEngine(){printf("creat BMW 320 Engine!\n");}
};
class BMW520Engine : public Engine
{
public:
    BMW520Engine(){this->CreatEngine();}
    void CreatEngine(){printf("creat BMW 520 Engine!\n");}
};
```
```cpp
//抽象变速箱配件类
class GearBox
{
public:
    virtual void CreatGearBox() = 0;
};
//具体变速箱配件类
class BMW320GearBox : public GearBox
{
public:
    BMW320GearBox(){this->CreatGearBox();}
    void CreatGearBox(){printf("creat BMW 320 GearBox!\n");}
};

class BMW520GearBox : public GearBox
{
public:
    BMW520GearBox(){this->CreatGearBox();}
    void CreatGearBox(){printf("creat BMW 520 GearBox!\n");}
};
```

```cpp
//抽象工厂类
class AbstractFactory
{
public:
    virtual Engine* CreatEngine() = 0;
    virtual GearBox* CreatGearBox() = 0;
};
//具体BMW320工厂类
class BMW320Factory : public AbstractFactory
{
public:
    BMW320Factory(){}
    Engine* CreatEngine(){
        return new BMW320Engine();
    }
    GearBox* CreatGearBox(){
        return new BMW320GearBox();
    }
};
//具体BMW520工厂类
class BMW520Factory : public AbstractFactory
{
public:
    BMW520Factory(){}
    Engine* CreatEngine(){
        return new BMW520Engine();
    }
    GearBox* CreatGearBox(){
        return new BMW520GearBox();
    }
};

```

```cpp
//客户端
#include <QCoreApplication>
#include "simplefactory.h"
#include "abstractfactory.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    AbstractFactory *factory320 = new BMW320Factory();
    AbstractFactory *factory520 = new BMW520Factory();
    //创建BMW320专属的Engine和Gearbox
    Engine *engine320 = factory320->CreatEngine();
    GearBox *gearbox320 = factory320->CreatGearBox();
    //创建BMW520专属的Engine和Gearbox
    Engine *engine520 = factory520->CreatEngine();
    GearBox *gearbox520 = factory520->CreatGearBox();
    return a.exec();
}


/*理论输出
creat BMW 320 Engine!
creat BMW 320 GearBox!
creat BMW 520 Engine!
creat BMW 520 GearBox!
*/

```

### 抽象工厂模式补充说明
其实这博客提到的三种工厂模式都非常的相似，都是为了实现将具体的产品与构造组合划分开来；不必为了追求更多的功能而使用相对难点的抽象工厂模式，最好的方法是根据需求来选用，如果简单工厂能实现现有需求及未来一段时间则采用这个模式，不能采用则采用其他模式。
　　抽象工厂模式关键在于对于归属于同一产品系列的配件做组合使用，这些配件可能分属于不用的抽象类，但在抽象工厂模式的具体工厂类中可以将他们做组合，正如BMW320Factory对 BMW320Engine 与 BMW320GearBox的组合实例化，这便是我认为抽象工厂的精髓。
