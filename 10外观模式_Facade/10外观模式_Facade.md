---
noteId: "ac802b4084fb11ea9333dbbab73676e0"
tags: []

---

## 外观模式（Facade模式）详解
在现实生活中，常常存在办事较复杂的例子，如办房产证或注册一家公司，有时要同多个部门联系，这时要是有一个综合部门能解决一切手续问题就好了。
软件设计也是这样，当一个系统的功能越来越强，子系统会越来越多，客户对系统的访问也变得越来越复杂。这时如果系统内部发生改变，客户端也要跟着改变，这违背了“开闭原则”，也违背了“迪米特法则”，所以有必要为多个子系统提供一个统一的接口，从而降低系统的耦合度，这就是外观模式的目标。
图 1 给出了客户去当地房产局办理房产证过户要遇到的相关部门
![办理房产证过户的相关部门](办理房产证过户的相关部门.gif)
### 外观模式的定义与特点
外观（Facade）模式的定义：是一种通过为多个复杂的子系统提供一个一致的接口，而使这些子系统更加容易被访问的模式。该模式对外有一个统一接口，外部应用程序不用关心内部子系统的具体的细节，这样会大大降低应用程序的复杂度，提高了程序的可维护性。
外观（Facade）模式是“迪米特法则”的典型应用，它有以下主要优点。
- 降低了子系统与客户端之间的耦合度，使得子系统的变化不会影响调用它的客户类。
- 对客户屏蔽了子系统组件，减少了客户处理的对象数目，并使得子系统使用起来更加容易。
- 降低了大型软件系统中的编译依赖性，简化了系统在不同平台之间的移植过程，因为编译一个子系统不会影响其他的子系统，也不会影响外观对象。
外观（Facade）模式的主要缺点如下。
- 不能很好地限制客户使用子系统类。
- 增加新的子系统可能需要修改外观类或客户端的源代码，违背了“开闭原则”。
### 外观模式的结构与实现
外观（Facade）模式的结构比较简单，主要是定义了一个高层接口。它包含了对各个子系统的引用，客户端可以通过它访问各个子系统的功能。现在来分析其基本结构和实现方法。
 1. 模式的结构
外观（Facade）模式包含以下主要角色。
- 外观（Facade）角色：为多个子系统对外提供一个共同的接口。
- 子系统（Sub System）角色：实现系统的部分功能，客户可以通过外观角色访问它。
- 客户（Client）角色：通过一个外观角色访问各个子系统的功能。
![外观Facade模式的结构图](外观Facade模式的结构图.gif)
2. 模式的实现
```java
    package facade;
    public class FacadePattern
    {
        public static void main(String[] args)
        {
            Facade f=new Facade();
            f.method();
        }
    }
    //外观角色
    class Facade
    {
        private SubSystem01 obj1=new SubSystem01();
        private SubSystem02 obj2=new SubSystem02();
        private SubSystem03 obj3=new SubSystem03();
        public void method()
        {
            obj1.method1();
            obj2.method2();
            obj3.method3();
        }
    }
    //子系统角色
    class SubSystem01
    {
        public  void method1()
        {
            System.out.println("子系统01的method1()被调用！");
        }   
    }
    //子系统角色
    class SubSystem02
    {
        public  void method2()
        {
            System.out.println("子系统02的method2()被调用！");
        }   
    }
    //子系统角色
    class SubSystem03
    {
        public  void method3()
        {
            System.out.println("子系统03的method3()被调用！");
        }   
    }

```
### 外观模式的应用实例
【例1】用“外观模式”设计一个婺源特产的选购界面。

分析：本实例的外观角色 WySpecialty 是 JPanel 的子类，它拥有 8 个子系统角色 Specialty1~Specialty8，它们是图标类（ImageIcon）的子类对象，用来保存该婺源特产的图标（点此下载要显示的婺源特产的图片）。

外观类（WySpecialty）用 JTree 组件来管理婺源特产的名称，并定义一个事件处理方法 valueClianged(TreeSelectionEvent e)，当用户从树中选择特产时，该特产的图标对象保存在标签（JLabd）对象中。

客户窗体对象用分割面板来实现，左边放外观角色的目录树，右边放显示所选特产图像的标签。其结构图如图 3 所示。
![婺源特产管理界面的结构图](婺源特产管理界面的结构图.gif)
```java
    package facade;
    import java.awt.*;
    import javax.swing.*;
    import javax.swing.event.*;
    import javax.swing.tree.DefaultMutableTreeNode;
    public class WySpecialtyFacade
    {
        public static void main(String[] args)
        {
            JFrame f=new JFrame ("外观模式: 婺源特产选择测试");
            Container cp=f.getContentPane();       
            WySpecialty wys=new WySpecialty();       
            JScrollPane treeView=new JScrollPane(wys.tree);
            JScrollPane scrollpane=new JScrollPane(wys.label);       
            JSplitPane splitpane=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,true,treeView,scrollpane); //分割面版
            splitpane.setDividerLocation(230);     //设置splitpane的分隔线位置
            splitpane.setOneTouchExpandable(true); //设置splitpane可以展开或收起                       
            cp.add(splitpane);
            f.setSize(650,350);
            f.setVisible(true);   
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
    class WySpecialty extends JPanel implements TreeSelectionListener
    {
        private static final long serialVersionUID=1L;
        final JTree tree;
        JLabel label;
        private Specialty1 s1=new Specialty1();
        private Specialty2 s2=new Specialty2();
        private Specialty3 s3=new Specialty3();
        private Specialty4 s4=new Specialty4();
        private Specialty5 s5=new Specialty5();
        private Specialty6 s6=new Specialty6();
        private Specialty7 s7=new Specialty7();
        private Specialty8 s8=new Specialty8();
        WySpecialty(){       
            DefaultMutableTreeNode top=new DefaultMutableTreeNode("婺源特产");
            DefaultMutableTreeNode node1=null,node2=null,tempNode=null;       
            node1=new DefaultMutableTreeNode("婺源四大特产（红、绿、黑、白）");
            tempNode=new DefaultMutableTreeNode("婺源荷包红鲤鱼");
            node1.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源绿茶");
            node1.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源龙尾砚");
            node1.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源江湾雪梨");
            node1.add(tempNode);
            top.add(node1);           
            node2=new DefaultMutableTreeNode("婺源其它土特产");
            tempNode=new DefaultMutableTreeNode("婺源酒糟鱼");
            node2.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源糟米子糕");
            node2.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源清明果");
            node2.add(tempNode);
            tempNode=new DefaultMutableTreeNode("婺源油煎灯");
            node2.add(tempNode);
            top.add(node2);           
            tree=new JTree(top);
            tree.addTreeSelectionListener(this);
            label=new JLabel();
        }   
        public void valueChanged(TreeSelectionEvent e)
        {
            if(e.getSource()==tree)
            {
                DefaultMutableTreeNode node=(DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                if(node==null) return;
                if(node.isLeaf())
                {
                    Object object=node.getUserObject();
                    String sele=object.toString();
                    label.setText(sele);
                    label.setHorizontalTextPosition(JLabel.CENTER);
                    label.setVerticalTextPosition(JLabel.BOTTOM);
                    sele=sele.substring(2,4);
                    if(sele.equalsIgnoreCase("荷包")) label.setIcon(s1);
                    else if(sele.equalsIgnoreCase("绿茶")) label.setIcon(s2);
                    else if(sele.equalsIgnoreCase("龙尾")) label.setIcon(s3);
                    else if(sele.equalsIgnoreCase("江湾")) label.setIcon(s4);
                    else if(sele.equalsIgnoreCase("酒糟")) label.setIcon(s5);
                    else if(sele.equalsIgnoreCase("糟米")) label.setIcon(s6);
                    else if(sele.equalsIgnoreCase("清明")) label.setIcon(s7);
                    else if(sele.equalsIgnoreCase("油煎")) label.setIcon(s8);
                    label.setHorizontalAlignment(JLabel.CENTER);
                }
            }               
        }
    }
    class Specialty1 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty1()
        {
            super("src/facade/WyImage/Specialty11.jpg");
        }
    }
    class Specialty2 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty2()
        {
            super("src/facade/WyImage/Specialty12.jpg");
        }
    }
    class Specialty3 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty3()
        {
            super("src/facade/WyImage/Specialty13.jpg");
        }
    }
    class Specialty4 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty4()
        {
            super("src/facade/WyImage/Specialty14.jpg");
        }
    }
    class Specialty5 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty5()
        {
            super("src/facade/WyImage/Specialty21.jpg");
        }
    }
    class Specialty6 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty6()
        {
            super("src/facade/WyImage/Specialty22.jpg");
        }
    }
    class Specialty7 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty7()
        {
            super("src/facade/WyImage/Specialty23.jpg");
        }
    }
    class Specialty8 extends ImageIcon
    {
        private static final long serialVersionUID=1L;
        Specialty8()
        {
            super("src/facade/WyImage/Specialty24.jpg");
        }
    }
```
![婺源特产管理界面的运行结果](婺源特产管理界面的运行结果.jpg)
### 外观模式的应用场景
通常在以下情况下可以考虑使用外观模式。
- 对分层结构系统构建时，使用外观模式定义子系统中每层的入口点可以简化子系统之间的依赖关系。
- 当一个复杂系统的子系统很多时，外观模式可以为系统设计一个简单的接口供外界访问。
- 当客户端与多个子系统之间存在很大的联系时，引入外观模式可将它们分离，从而提高子系统的独立性和可移植性。
### 外观模式的扩展
在外观模式中，当增加或移除子系统时需要修改外观类，这违背了“开闭原则”。如果引入抽象外观类，则在一定程度上解决了该问题，其结构图如图 5 所示。
![引入抽象外观类的外观模式的结构图](引入抽象外观类的外观模式的结构图.gif)

## CPP
摘要

 　　本章主要说明外观模式，该设计模式主要意图是：将一个具有统一特征流程性的东西封装成为一个外部接口，外部接口不需要了解内部实现，只需要调用接口，其余操作由接口函数内部完成。
　 　我们的产品有普通用户，也有专业用户，对于普通用户我们要提供给他尽量简单的使用方式，对专业用户他们可能会针对其中某一环节的结果做修改，那么这种简单自动流程接口与细节函数接口同时存在的情况就是外观模式。
　 　具体实现下边我们就通过一个小栗子来说明什么是外观模式。
主要参与者

该设计模式的参与者有5个，分别是：
- Facade （ShishKebab） 分配用户请求给适当的子系统对象
- Subsystem classes（ChoppedLamb， PickledLamb， BarbecueBraze， LambWithBarbecueBrazeObj， FinishedShishKebab）实现子系统的功能，处理由Facade 分配的任务，没有Facade 的任何相关信息
- Client 用户
优点
- 对客户屏蔽了系统组件，减少客户处理对象的数目并且使得系统使用更简单
- 实现了子系统与客户之间的松耦合关系，而子系统内部是紧耦合的，有助于建立层次结构系统，也有助于对象之间的依赖关系分层
- 虽然包装了一层，但并不限制使用子系统的类
### 具体实现代码
例子以烤羊肉串制作过程来说明，我们现在有切好的羊肉，首先对切好的羊肉进行腌制，洗干净穿羊肉的钎子，之后把羊肉穿到钎子上放在一起，最后经过烧烤完成美味的烤肉；
 　　外观模式就是准备好切好的羊肉，把羊肉交给专业烤肉师傅，他会烤好后返回给你烤好的羊肉串；如果你想自己体验烤肉过程的话，可以经过之前说到的步骤完整的完成烤肉过程，或从中间某一步比如说串好的烤肉继续后续步骤，接下来我们通过实例代码来说明具体实现：
对象的接口（Component ）
```cpp
#ifndef SHISHKEBAB_H
#define SHISHKEBAB_H
#include <stdio.h>
class ChoppedLamb   //羊肉原料
{
public:
    ChoppedLamb(){printf("Original chopped lamb raw.\n");}
};

class PickledLamb   //腌羊肉
{
public:
    PickledLamb(ChoppedLamb *choppedLamb = 0)
        :m_choppedLamb(choppedLamb){}
    void ProcessLamb(){printf("Pickled Lamb.\n");}
protected:
    ChoppedLamb *m_choppedLamb;
};

class BarbecueBraze //烤肉钎子
{
public:
    BarbecueBraze(){}
    void Wash(){printf("Clean Barbecue Braze.\n");}
};

class LambWithBarbecueBrazeObj //穿好的烤肉串
{
public:
    LambWithBarbecueBrazeObj(PickledLamb *pickledLamb = 0, BarbecueBraze *barbecueBraze = 0)
        :m_barbecueBraze(barbecueBraze),
          m_pickledLamb(pickledLamb){}

    void ProcessShishKebabObj(){printf("Skew the lamb with BarbecueBraze.\n");}
protected:
    BarbecueBraze *m_barbecueBraze;
    PickledLamb *m_pickledLamb;
};

class FinishedShishKebab    //烤好的羊肉
{
public:
    FinishedShishKebab(LambWithBarbecueBrazeObj *obj)
        :m_lambWithBarbecueBrazeObj(obj){}
    void Info(){printf("Got Finished Shish Kebab.\n");}
protected:
    LambWithBarbecueBrazeObj *m_lambWithBarbecueBrazeObj;
};

class ShishKebab    //烤羊肉
{
public:
    ShishKebab(){}
    void ProcessBarbecue(ChoppedLamb *inPut, FinishedShishKebab *outPut){
        PickledLamb *pickledLamb = new PickledLamb(inPut);
        BarbecueBraze *barbecueBraze = new BarbecueBraze;
        barbecueBraze->Wash();
        LambWithBarbecueBrazeObj *lambWithBarbecueBrazeObj =
                new LambWithBarbecueBrazeObj(pickledLamb,barbecueBraze);
        lambWithBarbecueBrazeObj->ProcessShishKebabObj();
        FinishedShishKebab *finishedShishKebab = new FinishedShishKebab(lambWithBarbecueBrazeObj);
        outPut = finishedShishKebab;
        printf("Barbecue the ShishKebabObj.\n");
    }
};

#endif // SHISHKEBAB_H
```
用户（Client）
```cpp
//Common User
#include <QCoreApplication>
#include "shishkebab.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //first: use Facade patten
    printf(" Facade patten.\n");
    ChoppedLamb *raw = new ChoppedLamb;
    FinishedShishKebab *product;
    ShishKebab *shishKebab = new ShishKebab();
    shishKebab->ProcessBarbecue(raw, product);
    product->Info();
    return a.exec();
}
```
![输出结果](输出结果.png)

```cpp
//professional User
#include <QCoreApplication>
#include "shishkebab.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    printf("\n Standard way.\n");
    ChoppedLamb *raw1 = new ChoppedLamb;
    PickledLamb *pickledLamb = new PickledLamb(raw1);
    BarbecueBraze *barbecueBraze = new BarbecueBraze;
    barbecueBraze->Wash();
    LambWithBarbecueBrazeObj *lambWithBarbecueBrazeObj =
            new LambWithBarbecueBrazeObj(pickledLamb,barbecueBraze);
    lambWithBarbecueBrazeObj->ProcessShishKebabObj();
    FinishedShishKebab *finishedShishKebab = new FinishedShishKebab(lambWithBarbecueBrazeObj);
    finishedShishKebab->Info();
    return a.exec();
}
```
输出结果2
![输出结果](输出结果.png)
补充说明
 　　该模式的思想是给外部客户尽量简单的使用接口，对下层子系统实现的流程进行封装，但同时又不限制用户对下层子系统类的使用

---

```cpp
// Facade.h
#ifndef _FACADE_H_
#define _FACADE_H_
class Subsystem1 {
 public:
  Subsystem1();
  ~Subsystem1();
  void Operation();

 protected:
 private:
};
class Subsystem2 {
 public:
  Subsystem2();
  ~Subsystem2();
  void Operation();

 protected:
 private:
};
class Facade {
 public:
  Facade();
  ~Facade();
  void OperationWrapper();

 protected:
 private:
  Subsystem1* _subs1;
  Subsystem2* _subs2;
};
#endif  //~_FACADE_H_
```

```cpp
// Facade.cpp
#include <iostream>

#include "Facade.h"
using namespace std;
Subsystem1::Subsystem1() {}
Subsystem1::~Subsystem1() {}
void Subsystem1::Operation() { cout << "Subsystem1 operation.." << endl; }
Subsystem2::Subsystem2() {}
Subsystem2::~Subsystem2() {}
void Subsystem2::Operation() { cout << "Subsystem2 operation.." << endl; }
Facade::Facade() {
  this->_subs1 = new Subsystem1();
  this->_subs2 = new Subsystem2();
}
Facade::~Facade() {
  delete _subs1;
  delete _subs2;
}
void Facade::OperationWrapper() {
  this->_subs1->Operation();
  this->_subs2->Operation();
}
```

```cpp
// main.cpp
#include <iostream>

#include "Facade.h"
using namespace std;
int main(int argc, char* argv[]) {
  Facade* f = new Facade();
  f->OperationWrapper();
  return 0;
}
```