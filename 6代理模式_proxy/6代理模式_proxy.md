---
noteId: "1c56b170837411eab28dab27f2a3be8b"
tags: []

---

## 代理模式（代理设计模式）详解
在有些情况下，一个客户不能或者不想直接访问另一个对象，这时需要找一个中介帮忙完成某项任务，这个中介就是代理对象。例如，购买火车票不一定要去火车站买，可以通过 12306 网站或者去火车票代售点买。又如找女朋友、找保姆、找工作等都可以通过找中介完成。

在软件设计中，使用代理模式的例子也很多，例如，要访问的远程对象比较大（如视频或大图像等），其下载要花很多时间。还有因为安全原因需要屏蔽客户端直接访问真实对象，如某单位的内部数据库等。

### 代理模式的定义与特点
代理模式的定义：由于某些原因需要给某对象提供一个代理以控制对该对象的访问。这时，访问对象不适合或者不能直接引用目标对象，代理对象作为访问对象和目标对象之间的中介。

代理模式的主要优点有：
    代理模式在客户端与目标对象之间起到一个中介作用和保护目标对象的作用；
    代理对象可以扩展目标对象的功能；
    代理模式能将客户端与目标对象分离，在一定程度上降低了系统的耦合度；

其主要缺点是：
    在客户端和目标对象之间增加一个代理对象，会造成请求处理速度变慢；
    增加了系统的复杂度；

### 代理模式的结构与实现
代理模式的结构比较简单，主要是通过定义一个继承抽象主题的代理来包含真实主题，从而实现对真实主题的访问，下面来分析其基本结构和实现方法。

1. 模式的结构
代理模式的主要角色如下。
> 1. 抽象主题（Subject）类：通过接口或抽象类声明真实主题和代理对象实现的业务方法。
> 2. 真实主题（Real Subject）类：实现了抽象主题中的具体业务，是代理对象所代表的真实对象，是最终要引用的对象。
> 3. 代理（Proxy）类：提供了与真实主题相同的接口，其内部含有对真实主题的引用，它可以访问、控制或扩展真实主题的功能。  

![代理模式的结构图](代理模式的结构图.gif)

2. 模式的实现
```java
    package proxy;
    public class ProxyTest
    {
        public static void main(String[] args)
        {
            Proxy proxy=new Proxy();
            proxy.Request();
        }
    }
    //抽象主题
    interface Subject
    {
        void Request();
    }
    //真实主题
    class RealSubject implements Subject
    {
        public void Request()
        {
            System.out.println("访问真实主题方法...");
        }
    }
    //代理
    class Proxy implements Subject
    {
        private RealSubject realSubject;
        public void Request()
        {
            if (realSubject==null)
            {
                realSubject=new RealSubject();
            }
            preRequest();
            realSubject.Request();
            postRequest();
        }
        public void preRequest()
        {
            System.out.println("访问真实主题之前的预处理。");
        }
        public void postRequest()
        {
            System.out.println("访问真实主题之后的后续处理。");
        }
    }
```
```
访问真实主题之前的预处理。
访问真实主题方法...
访问真实主题之后的后续处理。
```

### 代理模式的应用实例
【例1】韶关“天街e角”公司是一家婺源特产公司的代理公司，用代理模式实现。
分析：本实例中的“婺源特产公司”经营许多婺源特产，它是真实主题，提供了显示特产的 display() 方法，可以用窗体程序实现（点此下载该实例所要显示的图片）。而韶关“天街e角”公司是婺源特产公司特产的代理，通过调用婺源特产公司的 display() 方法显示代理产品，当然它可以增加一些额外的处理，如包裝或加价等。客户可通过“天街e角”代理公司间接访问“婺源特产公司”的产品，图 2 所示是公司的结构图。
![公司的结构图](公司的结构图.gif)
```java
    package proxy;
    import java.awt.*;
    import javax.swing.*;
    public class WySpecialtyProxy
    {
        public static void main(String[] args)
        {
            SgProxy proxy=new SgProxy();
            proxy.display();
        }
    }
    //抽象主题：特产
    interface Specialty
    {
        void display();
    }
    //真实主题：婺源特产
    class WySpecialty extends JFrame implements Specialty
    {
        private static final long serialVersionUID=1L;
        public WySpecialty()
        {
            super("韶关代理婺源特产测试");
            this.setLayout(new GridLayout(1,1));
            JLabel l1=new JLabel(new ImageIcon("src/proxy/WuyuanSpecialty.jpg"));
            this.add(l1);   
            this.pack();       
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);      
        }
        public void display()
        {
            this.setVisible(true);
        }
    }
    //代理：韶关代理
    class SgProxy implements Specialty
    {
        private WySpecialty realSubject=new WySpecialty();
        public void display()
        {
            preRequest();
            realSubject.display();
            postRequest();
        }
        public void preRequest()
        {
              System.out.println("韶关代理婺源特产开始。");
        }
        public void postRequest()
        {
              System.out.println("韶关代理婺源特产结束。");
        }
    }
```
![公司的代理产品](公司的代理产品.jpg)

### 代理模式的应用场景
前面分析了代理模式的结构与特点，现在来分析以下的应用场景。
- 远程代理，这种方式通常是为了隐藏目标对象存在于不同地址空间的事实，方便客户端访问。例如，用户申请某些网盘空间时，会在用户的文件系统中建立一个虚拟的硬盘，用户访问虚拟硬盘时实际访问的是网盘空间。
- 虚拟代理，这种方式通常用于要创建的目标对象开销很大时。例如，下载一幅很大的图像需要很长时间，因某种计算比较复杂而短时间无法完成，这时可以先用小比例的虚拟代理替换真实的对象，消除用户对服务器慢的感觉。
- 安全代理，这种方式通常用于控制不同种类客户对真实对象的访问权限。
- 智能指引，主要用于调用目标对象时，代理附加一些额外的处理功能。例如，增加计算真实对象的引用次数的功能，这样当该对象没有被引用时，就可以自动释放它。
- 延迟加载，指为了提高系统的性能，延迟对目标的加载。例如，Hibernate 中就存在属性的延迟加载和关联表的延时加载。

### 代理模式的扩展
在前面介绍的代理模式中，代理类中包含了对真实主题的引用，这种方式存在两个缺点。
- 真实主题与代理主题一一对应，增加真实主题也要增加代理。
- 设计代理以前真实主题必须事先存在，不太灵活。采用动态代理模式可以解决以上问题，如 SpringAOP，其结构图如图 4 所示。
![动态代理模式的结构图](动态代理模式的结构图.gif)

## CPP

章主要说明代理模式，该设计模式主要意图是：在需要时才创建和初始化，不需要同时创建这些对象。
代理模式有以下四种常见情况：
- 远程代理
- 虚代理
- 保护代理
- 智能引用
具体实现下边我们就通过以下的栗子来说明什么是代理模式。

### 主要参与者
该设计模式的参与者有4个，分别是：
- Subject（PhotoInfo） 接口基类
- Proxy（BigPhotoProxy） 通过该对象来实现对被代理对象的访问或控制
- RealSubject（BigPhoto）定义Proxy所代表的实体对象
- Client 用户

### 优点
虚代理
- 根据需求来控制是否创建实例对象
- 在实例对象与外部使用接口之间加一层缓冲，增加安全性

### 具体实现代码
代理模式中的虚代理以大图片加载为例，当我们在访问网站时是否常遇到有些图片未加载时显示一个小图标或压缩图片来替代原来位置，当点击下查看原图或稍等一会就会加载出原始高清图片，未加载时的小图片其实就可以通过代理模式实现，原图则是被代理的实例对象，通过点击或一定时间处理来创建或加载。

接口基类（Subject）
```cpp
#ifndef PHOTOINFO_H
#define PHOTOINFO_H
/****************************************************************
 Doc    :   photoinfo.h
 Author :   BingLee
 Date   :   2019-12-23
 Info   :   Proxy Design Patten
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#include <string>
using namespace std;
//This is a class include photo base model information
class PhotoInfo
{
public:
    PhotoInfo(){}
//    virtual ~PhotoInfo(){}
    virtual void GetRealPhoto(){}
protected:
    string m_Photo;
};

```
被代理的实体对象（RealSubject）
```cpp
//This is a real big photo with "m_Photo"
class BigPhoto : public PhotoInfo
{
public:
    BigPhoto(string photo){m_Photo = photo;}
    ~BigPhoto(){}
    void GetRealPhoto()
    {
        printf("Get real ");
        printf(m_Photo.c_str());
        printf(" for view ! \n");
    }
};
```
代理（Proxy）
```cpp
//This is BigPhoto proxy
class BigPhotoProxy : public PhotoInfo
{
public:
    BigPhotoProxy(string photo):m_pBigPhoto(NULL){
        //Befor askreal photo for view, see GetRealPhoto get some base info
        m_Photo = photo;
        printf("This is a proxy of %s.\n",m_Photo.c_str());
    }
    ~BigPhotoProxy(){}
    void GetRealPhoto()
    {
        //ask real photo to view
        if(m_pBigPhoto == NULL)
        {
            m_pBigPhoto = new BigPhoto(m_Photo);
        }
        m_pBigPhoto->GetRealPhoto();
    }
private:
    BigPhoto *m_pBigPhoto;
};

#endif // PHOTOINFO_H
```
用户（Client）
```cpp
/****************************************************************
 Doc    :   main.cpp
 Author :   BingLee
 Date   :   2019-12-23
 Info   :   Proxy Design Patten
 https://blog.csdn.net/Bing_Lee (C)All rights reserved.
******************************************************************/
#include <QCoreApplication>
#include "photoinfo.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    BigPhotoProxy *pBigPhotoProxy = new BigPhotoProxy("Planet Photo");
    pBigPhotoProxy->GetRealPhoto();

    return a.exec();
}

```
![输出结果](输出结果.jpg)

#### 补充说明
该模式当前只写了一个虚代理的实现，虚代理可进行最优化控制，根据需求来创建或加载实例对象。


---

至少在以下集中情况下可以用 Proxy 模式解决问题：
1）创建开销大的对象时候，比如显示一幅大的图片，我们将这个创建的过程交给代理
去完成， GoF 称之为虚代理（ Virtual Proxy）；
2）为网络上的对象创建一个局部的本地代理， 比如要操作一个网络上的一个对象（ 网
络性能不好的时候，问题尤其突出），我们将这个操纵的过程交给一个代理去完成， GoF 称
之为远程代理（ Remote Proxy）；
3） 对对象进行控制访问的时候， 比如在 Jive 论坛中不同权限的用户（如管理员、 普通
用户等） 将获得不同层次的操作权限， 我们将这个工作交给一个代理去完成， GoF 称之为保
护代理（ Protection Proxy）
4） 智能指针（ Smart Pointer）， 关于这个方面的内容， 建议参看 Andrew Koenig 的《C++沉思录》中的第 5 章。

```cpp
//Proxy.h
#ifndef _PROXY_H_
#define _PROXY_H_
class Subject
{
public:
    virtual ~Subject();
    virtual void Request() = 0;
protected:
    Subject();
private:
};
class ConcreteSubject:public Subject
{
public:
    ConcreteSubject();
    ~ConcreteSubject();
    void Request();
protected:
private:
};
class Proxy
{
public:
    Proxy();
    Proxy(Subject* sub);
    ~Proxy();
    void Request();
protected:
private:
    Subject* _sub;
};
#endif //~_PROXY_H_
```

```cpp
//Proxy.cpp
#include "Proxy.h"
#include <iostream>
using namespace std;
Subject::Subject(){}
Subject::~Subject(){}
ConcreteSubject::ConcreteSubject(){}
ConcreteSubject::~ConcreteSubject(){}
void ConcreteSubject::Request(){
    cout<<"ConcreteSubject......request...."<<endl;
}
Proxy::Proxy(){}
Proxy::Proxy(Subject* sub){
    _sub = sub;
}
Proxy::~Proxy(){
    delete _sub;
}
void Proxy::Request(){
    cout<<"Proxy request...."<<endl;
    _sub->Request();
}
```

```cpp
//main.cpp
#include "Proxy.h"
#include <iostream>
using namespace std;
int main(int argc,char* argv[])
{
    Subject* sub = new ConcreteSubject();
    Proxy* p = new Proxy(sub);
    p->Request();
    return 0;
}
```