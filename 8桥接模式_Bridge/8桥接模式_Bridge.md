---
noteId: "f09dd6e0844411eaa06473d2bf67c198"
tags: []

---

## 桥接模式（Bridge模式）



在现实生活中，某些类具有两个或多个维度的变化，如图形既可按形状分，又可按颜色分。如何设计类似于 Photoshop 这样的软件，能画不同形状和不同颜色的图形呢？如果用继承方式，m 种形状和 n 种颜色的图形就有 m×n 种，不但对应的子类很多，而且扩展困难。

当然，这样的例子还有很多，如不同颜色和字体的文字、不同品牌和功率的汽车、不同性别和职业的男女、支持不同平台和不同文件格式的媒体播放器等。如果用桥接模式就能很好地解决这些问题。

### 桥接模式的定义与特点
桥接（Bridge）模式的定义如下：将抽象与实现分离，使它们可以独立变化。它是用组合关系代替继承关系来实现，从而降低了抽象和实现这两个可变维度的耦合度。

桥接（Bridge）模式的优点是：
- 由于抽象与实现分离，所以扩展能力强；
- 其实现细节对客户透明。

缺点是：由于聚合关系建立在抽象层，要求开发者针对抽象化进行设计与编程，这增加了系统的理解与设计难度。

### 桥接模式的结构与实现
可以将抽象化部分与实现化部分分开，取消二者的继承关系，改用组合关系。
 1. 模式的结构
桥接（Bridge）模式包含以下主要角色。
- 抽象化（Abstraction）角色：定义抽象类，并包含一个对实现化对象的引用。
- 扩展抽象化（Refined    Abstraction）角色：是抽象化角色的子类，实现父类中的业务方法，并通过组合关系调用实现化角色中的业务方法。
- 实现化（Implementor）角色：定义实现化角色的接口，供扩展抽象化角色调用。
- 具体实现化（Concrete Implementor）角色：给出实现化角色接口的具体实现。
![桥接模式的结构图](桥接模式的结构图.gif)

2. 模式的实现
```java
    package bridge;
    public class BridgeTest
    {
        public static void main(String[] args)
        {
            Implementor imple=new ConcreteImplementorA();
            Abstraction abs=new RefinedAbstraction(imple);
            abs.Operation();
        }
    }
    //实现化角色
    interface Implementor
    {
        public void OperationImpl();
    }
    //具体实现化角色
    class ConcreteImplementorA implements Implementor
    {
        public void OperationImpl()
        {
            System.out.println("具体实现化(Concrete Implementor)角色被访问" );
        }
    }
    //抽象化角色
    abstract class Abstraction
    {
       protected Implementor imple;
       protected Abstraction(Implementor imple)
       {
           this.imple=imple;
       }
       public abstract void Operation();   
    }
    //扩展抽象化角色
    class RefinedAbstraction extends Abstraction
    {
       protected RefinedAbstraction(Implementor imple)
       {
           super(imple);
       }
       public void Operation()
       {
           System.out.println("扩展抽象化(Refined Abstraction)角色被访问" );
           imple.OperationImpl();
       }
    }
```
```
扩展抽象化(Refined Abstraction)角色被访问
具体实现化(Concrete Implementor)角色被访问
```
### 桥接模式的应用实例
【例1】用桥接（Bridge）模式模拟女士皮包的选购。

分析：女士皮包有很多种，可以按用途分、按皮质分、按品牌分、按颜色分、按大小分等，存在多个维度的变化，所以采用桥接模式来实现女士皮包的选购比较合适。

本实例按用途分可选钱包（Wallet）和挎包（HandBag），按颜色分可选黄色（Yellow）和红色（Red）。可以按两个维度定义为颜色类和包类。（点此下载本实例所要显示的包的图片）。

颜色类（Color）是一个维度，定义为实现化角色，它有两个具体实现化角色：黄色和红色，通过 getColor() 方法可以选择颜色；包类（Bag）是另一个维度，定义为抽象化角色，它有两个扩展抽象化角色：挎包和钱包，它包含了颜色类对象，通过 getName() 方法可以选择相关颜色的挎包和钱包。

客户类通过 ReadXML 类从 XML 配置文件中获取包信息（点此下载 XML 配置文件），并把选到的产品通过窗体显示出现，图 2 所示是其结构图。
![女士皮包选购的结构图](女士皮包选购的结构图.gif)

```java
    package bridge;
    import java.awt.*;
    import javax.swing.*;
    public class BagManage
    {
        public static void main(String[] args)
        {
            Color color;
            Bag bag;
            color=(Color)ReadXML.getObject("color");
            bag=(Bag)ReadXML.getObject("bag");
            bag.setColor(color);
            String name=bag.getName();
            show(name);
        }
        public static void show(String name)
        {
            JFrame jf=new JFrame("桥接模式测试");
            Container contentPane=jf.getContentPane();
            JPanel p=new JPanel();   
            JLabel l=new JLabel(new ImageIcon("src/bridge/"+name+".jpg"));
            p.setLayout(new GridLayout(1,1));
            p.setBorder(BorderFactory.createTitledBorder("女士皮包"));
            p.add(l);
            contentPane.add(p, BorderLayout.CENTER);
            jf.pack();  
            jf.setVisible(true);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
    //实现化角色：颜色
    interface Color
    {
        String getColor();
    }
    //具体实现化角色：黄色
    class Yellow implements Color
    {
        public String getColor()
        {
            return "yellow";
        }
    }
    //具体实现化角色：红色
    class Red implements Color
    {
        public String getColor()
        {
            return "red";
        }
    }
    //抽象化角色：包
    abstract class Bag
    {
        protected Color color;
        public void setColor(Color color)
        {
            this.color=color;
        }   
        public abstract String getName();
    }
    //扩展抽象化角色：挎包
    class HandBag extends Bag
    {
        public String getName()
        {
            return color.getColor()+"HandBag";
        }   
    }
    //扩展抽象化角色：钱包
    class Wallet extends Bag
    {
        public String getName()
        {
            return color.getColor()+"Wallet";
        }   
    }
```
```java
    package bridge;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import java.io.*;
    class ReadXML
    {
        public static Object getObject(String args)
        {
            try
            {
                DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
                DocumentBuilder builder=dFactory.newDocumentBuilder();
                Document doc;                           
                doc=builder.parse(new File("src/bridge/config.xml"));
                NodeList nl=doc.getElementsByTagName("className");
                Node classNode=null;
                if(args.equals("color"))
                {
                    classNode=nl.item(0).getFirstChild();
                }
                else if(args.equals("bag"))
                {
                    classNode=nl.item(1).getFirstChild();
                }          
                String cName="bridge."+classNode.getNodeValue();
                Class<?> c=Class.forName(cName);
                  Object obj=c.newInstance();
                return obj;
            }  
            catch(Exception e)
            {
                   e.printStackTrace();
                   return null;
            }
        }
    }
```
![女士皮包选购的运行结果1](女士皮包选购的运行结果1.jpg)

```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <config>
        <className>Red</className>
        <className>Wallet</className>
    </config>
```
则程序的运行结果如图 4 所示。
![女士皮包选购的运行结果2](女士皮包选购的运行结果2.jpg)

### 桥接模式的应用场景
桥接模式通常适用于以下场景。
    当一个类存在两个独立变化的维度，且这两个维度都需要进行扩展时。
    当一个系统不希望使用继承或因为多层次继承导致系统类的个数急剧增加时。
    当一个系统需要在构件的抽象化角色和具体化角色之间增加更多的灵活性时。

### 桥接模式模式的扩展
在软件开发中，有时桥接（Bridge）模式可与适配器模式联合使用。当桥接（Bridge）模式的实现化角色的接口与现有类的接口不一致时，可以在二者中间定义一个适配器将二者连接起来，其具体结构图如图 所示。
![桥接模式与适配器模式联用的结构图](桥接模式与适配器模式联用的结构图.gif)

## CPP

本章主要说明桥接模式，该设计模式主要意图是：将抽象部分与实现部分分离开来，使它们都可以独立的变化。
　 　如果了解过模板模式的话可以通过与模板模式的比较来学习和理解桥接模式，从某种程度上来说桥接模式可以称作模板模式的进阶版，模板模式是将通用接口虚函数化，通过子类对虚函数接口重载实现，这样就不需要定义大量的函数污染函数空间，也更容易理解。而桥接模式与模板模式的不同在于桥接模式多了一个虚拟的桥接类，通过这个类的继承类来完成具体实现下边我们就通过一个小栗子来说明什么是桥接模式。
主要参与者

该设计模式的参与者有5个，分别是：
- Abstraction 抽象类的接口
- RedefineAbstraction 扩充抽象类的接口
- Implementor 定义实现类的抽象接口
- ConcreteImplementor 具体实现抽象接口的类
- Client 用户

优点
- 分离接口与其实现部分；
- 提高可扩充性，使用更灵活；
- 实现细节对客户透明；（当然你也可以在实现时通过共享或引用机制隐藏这些细节）

### 具体实现代码

 　　例子中Abstraction 是抽象笔记本电脑，RedefineAbstraction 是不同品牌的笔记本电脑，Implementor 是抽象操作系统，ConcreteImplementor 是具体的操作系统，我们在选择电脑时可以选择不同的电脑品牌，但与此同时我们还需要选择所安装的操作系统，这样我们将买不同电脑品牌和安装不同操作系统的过程分离出来，这大致就是桥接模式的意图，接下来我们通过一个实例代码来说明具体实现：  
 抽象类接口（Abstraction ）
 ```cpp
#ifndef COMPUTER_H
#define COMPUTER_H

#include "OS.h"
class Computer
{
public:
    virtual void InstallOS(OS *os) = 0;
};
#endif // COMPUTER_H
```
扩充抽象类的接口（RedefineAbstraction ）
```cpp
#ifndef DELLCOMPUTER_H
#define DELLCOMPUTER_H
#include "computer.h"
#include "os.h"
class DellComputer : public Computer
{
public:
    void InstallOS(OS *os)
    {
        printf("This is DellComputer");
        os->InitOS();
    }
};

class AppleComputer : public Computer
{
public:
    void InstallOS(OS *os)
    {
        printf("This is AppleComputer");
        os->InitOS();
    }
};
#endif // DELLCOMPUTER_H
```

定义实现类的抽象接口（Implementor ）
```cpp
#ifndef OS_H
#define OS_H
class OS
{
public:
    virtual void InitOS() = 0;
};
#endif // OS_H
```
具体实现抽象接口的类（ConcreteImplementor ）
```cpp
#ifndef CONCRETEOS_H
#define CONCRETEOS_H
#include "os.h"
#include "stdio.h"
class Linux : public OS
{
public:
    void InitOS(){printf(" Install Linux.\n");}
};
class Windows : public OS
{
public:
    void InitOS(){printf(" Install Windows.\n");}
};
class MacOS : public OS
{
public:
    void InitOS(){printf(" Install MacOS.\n");}
};
#endif // CONCRETEOS_H
```
用户使用（Client）
```cpp
#include <QCoreApplication>
#include "concreteos.h"
#include "redefinecomputer.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //具体操作系统
    OS *osLinux = new Linux();
    OS *osWindows = new Windows();
    OS *osMac = new MacOS();
    //具体笔记本电脑
    Computer *dellComputer1 = new DellComputer();
    Computer *dellComputer2 = new DellComputer();
    Computer *appleComputer1 = new AppleComputer();
    Computer *appleComputer2 = new AppleComputer();
    //安装操作系统
    dellComputer1->InstallOS(osLinux);
    dellComputer2->InstallOS(osWindows);
    appleComputer1->InstallOS(osMac);
    appleComputer2->InstallOS(osWindows);

    return a.exec();
}
```
输出结果3
![输出结果3](输出结果3.png)

## Bridge 模式
总结面向对象实际上就两句话：一是松耦合（ Coupling），二是高内聚（ Cohesion）。面向对象系统追求的目标就是尽可能地提高系统模块内部的内聚（ Cohesion）、尽可能降低模块间的耦合（ Coupling）。然而这也是面向对象设计过程中最为难把握的部分，大家肯定在OO 系统的开发过程中遇到这样的问题：

1）客户给了你一个需求，于是使用一个类来实现（ A）；
2）客户需求变化，有两个算法实现功能，于是改变设计， 我们通过一个抽象的基类，
再定义两个具体类实现两个不同的算法（ A1 和 A2）；
3）客户又告诉我们说对于不同的操作系统，于是再抽象一个层次，作为一个抽象基类
A0，在分别为每个操作系统派生具体类（ A00 和 A01，其中 A00 表示原来的类 A）实现不
同操作系统上的客户需求，这样我们就有了一共 4 个类。
4）可能用户的需求又有变化，比如说又有了一种新的算法……..
5）我们陷入了一个需求变化的郁闷当中，也因此带来了类的迅速膨胀。
Bridge 模式则正是解决了这类问题。
![Bridge_Pattern_结构图](Bridge_Pattern_结构图.png)

在 Bridge 模式的结构图中可以看到，系统被分为两个相对独立的部分，左边是抽象部分，右边是实现部分，这两个部分可以互相独立地进行修改：例如上面问题中的客户需求变化，当用户需求需要从 Abstraction 派生一个具体子类时候，并不需要像上面通过继承方式实现时候需要添加子类 A1 和 A2 了。另外当上面问题中由于算法添加也只用改变右边实现（添加一个具体化子类），而右边不用在变化，也不用添加具体子类了
一切都变得 elegant！
#### 实现
```cpp
// Abstraction.h
#ifndef _ABSTRACTION_H_
#define _ABSTRACTION_H_
class AbstractionImp;
class Abstraction {
 public:
  virtual ~Abstraction();
  virtual void Operation() = 0;

 protected:
  Abstraction();

 private:
};
class RefinedAbstraction : public Abstraction {
 public:
  RefinedAbstraction(AbstractionImp* imp);
  ~RefinedAbstraction();
  void Operation();

 protected:
 private:
  AbstractionImp* _imp;
};
#endif  //~_ABSTRACTION_H_
```

```cpp
// Abstraction.cpp
#include <iostream>

#include "Abstraction.h"
#include "AbstractionImp.h"
using namespace std;
Abstraction::Abstraction() {}
Abstraction::~Abstraction() {}
RefinedAbstraction::RefinedAbstraction(Abstra ctionImp* imp) { _imp = imp; }
RefinedAbstraction::~RefinedAbstraction() {}
void RefinedAbstraction::Operation() { _imp->Operation(); }
```

```cpp
// AbstractionImp.h
#ifndef _ABSTRACTIONIMP_H_
#define _ABSTRACTIONIMP_H_
class AbstractionImp {
 public:
  virtual ~AbstractionImp();
  virtual void Operation() = 0;

 protected:
  AbstractionImp();

 private:
};
class ConcreteAbstractionImpA : public AbstractionImp {
 public:
  ConcreteAbstractionImpA();
  ~ConcreteAbstractionImpA();
  virtual void Operation();

 protected:
 private:
};
class ConcreteAbstractionImpB : public AbstractionImp {
 public:
  ConcreteAbstractionImpB();
  ~ConcreteAbstractionImpB();
  virtual void Operation();

 protected:
 private:
};
#endif  //~_ABSTRACTIONIMP_H_
```

```cpp
// AbstractionImp.cpp
#include <iostream>

#include "AbstractionImp.h"
using namespace std;
AbstractionImp::AbstractionImp() {}
AbstractionImp::~AbstractionImp() {}
void AbstractionImp::Operation() { cout << "AbstractionImp....imp..." << endl; }
ConcreteAbstractionImpA::ConcreteAbstractio nImpA() {}
ConcreteAbstractionImpA::~ConcreteAbstracti onImpA() {}
void ConcreteAbstractionImpA::Operation() {
  cout << "ConcreteAbstractionImpA...." << endl;
}
ConcreteAbstractionImpB::ConcreteAbstractio nImpB() {}
ConcreteAbstractionImpB::~ConcreteAbstracti onImpB() {}
void ConcreteAbstractionImpB::Operation() {
  cout << "ConcreteAbstractionImpB...." << endl;
}
```
```cpp
// main.cpp
#include <iostream>
#include "Abstraction.h"
#include "AbstractionImp.h"
using namespace std;
int main(int argc, char* argv[]) {
  AbstractionImp* imp = new ConcreteAbstractionImpA();
  Abstraction* abs = new RefinedAbstraction(imp);
  abs->Operation();
  return 0;
}
```

Bridge 是设计模式中比较复杂和难理解的模式之一， 也是 OO 开发与设计中经常会用到的模式之一。 使用组合（ 委托） 的方式将抽象和实现彻底地解耦， 这样的好处是抽象和实现可以分别独立地变化，系统的耦合性也得到了很好的降低。GoF 在说明 Bridge 模式时，在意图中指出 Bridge 模式“将抽象部分与它的实现部分分离， 使得它们可以独立地变化”。 这句话很简单， 但是也很复杂， 连 Bruce Eckel 在他的大作《Thinking in Patterns》中说“ Bridge 模式是 GoF 所讲述得最不好（ Poorly－ described）的模式”， 个人觉得也正是如此。 原因就在于 GoF 的那句话中的“实现” 该怎么去理解： “实现”特别是和“抽象” 放在一起的时候我们“默认” 的理解是“实现” 就是“抽象” 的具体子类的实现，但是这里 GoF 所谓的“实现”的含义不是指抽象基类的具体子类对抽象基类中虚函数（接口） 的实现， 是和继承结合在一起的。 而这里的“实现” 的含义指的是怎么去实现用户的需求， 并且指的是通过组合（委托） 的方式实现的， 因此这里的实现不是指的继承基类、实现基类接口，而是指的是通过对象组合实现用户的需求。 理解了这一点也就理解了Bridge 模式，理解了 Bridge 模式，你的设计就会更加 Elegant 了。实际上上面使用 Bridge 模式和使用带来问题方式的解决方案的根本区别在于是通过继承还是通过组合的方式去实现一个功能需求。因此面向对象分析和设计中有一个原则就是：Favor Composition Over Inheritance。 其原因也正在这里。