---
noteId: "5348b830845611eaa06473d2bf67c198"
tags: []

---

### 装饰模式（装饰设计模式）详解

在现实生活中，常常需要对现有产品增加新的功能或美化其外观，如房子装修、相片加相框等。在软件开发过程中，有时想用一些现存的组件。这些组件可能只是完成了一些核心功能。但在不改变其结构的情况下，可以动态地扩展其功能。所有这些都可以釆用装饰模式来实现。

### 装饰模式的定义与特点
装饰（Decorator）模式的定义：指在不改变现有对象结构的情况下，动态地给该对象增加一些职责（即增加其额外功能）的模式，它属于对象结构型模式。

装饰（Decorator）模式的主要优点有：
    采用装饰模式扩展对象的功能比采用继承方式更加灵活。
    可以设计出多个不同的具体装饰类，创造出多个不同行为的组合

其主要缺点是：装饰模式增加了许多子类，如果过度使用会使程序变得很复杂。

### 装饰模式的结构与实现
通常情况下，扩展一个类的功能会使用继承方式来实现。但继承具有静态特征，耦合度高，并且随着扩展功能的增多，子类会很膨胀。如果使用组合关系来创建一个包装对象（即装饰对象）来包裹真实对象，并在保持真实对象的类结构不变的前提下，为其提供额外的功能，这就是装饰模式的目标。下面来分析其基本结构和实现方法。
1. 模式的结构
装饰模式主要包含以下角色。
- 抽象构件（Component）角色：定义一个抽象接口以规范准备接收附加责任的对象。
- 具体构件（Concrete    Component）角色：实现抽象构件，通过装饰角色为其添加一些职责。
- 抽象装饰（Decorator）角色：继承抽象构件，并包含具体构件的实例，可以通过其子类扩展具体构件的功能。
- 具体装饰（ConcreteDecorator）角色：实现抽象装饰的相关方法，并给具体构件对象添加附加的责任。
![装饰模式的结构图](装饰模式的结构图.gif)

 2. 模式的实现
装饰模式的实现代码如下： 
```java
    package decorator;
    public class DecoratorPattern
    {
        public static void main(String[] args)
        {
            Component p=new ConcreteComponent();
            p.operation();
            System.out.println("---------------------------------");
            Component d=new ConcreteDecorator(p);
            d.operation();
        }
    }
    //抽象构件角色
    interface  Component
    {
        public void operation();
    }
    //具体构件角色
    class ConcreteComponent implements Component
    {
        public ConcreteComponent()
        {
            System.out.println("创建具体构件角色");       
        }   
        public void operation()
        {
            System.out.println("调用具体构件角色的方法operation()");           
        }
    }
    //抽象装饰角色
    class Decorator implements Component
    {
        private Component component;   
        public Decorator(Component component)
        {
            this.component=component;
        }   
        public void operation()
        {
            component.operation();
        }
    }
    //具体装饰角色
    class ConcreteDecorator extends Decorator
    {
        public ConcreteDecorator(Component component)
        {
            super(component);
        }   
        public void operation()
        {
            super.operation();
            addedFunction();
        }
        public void addedFunction()
        {
            System.out.println("为具体构件角色增加额外的功能addedFunction()");           
        }
    }
```

```
创建具体构件角色
调用具体构件角色的方法operation()
---------------------------------
调用具体构件角色的方法operation()
为具体构件角色增加额外的功能addedFunction()
```
### 装饰模式的应用实例
【例1】用装饰模式实现游戏角色“莫莉卡·安斯兰”的变身。
分析：在《恶魔战士》中，游戏角色“莫莉卡·安斯兰”的原身是一个可爱少女，但当她变身时，会变成头顶及背部延伸出蝙蝠状飞翼的女妖，当然她还可以变为穿着漂亮外衣的少女。这些都可用装饰模式来实现，在本实例中的“莫莉卡”原身有 setImage(String t) 方法决定其显示方式，而其 变身“蝙蝠状女妖”和“着装少女”可以用 setChanger() 方法来改变其外观，原身与变身后的效果用 display() 方法来显示（点此下载其原身和变身后的图片），图 2 所示是其结构图。
![游戏角色“莫莉卡·安斯兰”的结构图](游戏角色“莫莉卡·安斯兰”的结构图.gif)
```java
    package decorator;
    import java.awt.*;
    import javax.swing.*;
    public class MorriganAensland
    {
        public static void main(String[] args)
        {
            Morrigan m0=new original();
            m0.display();
            Morrigan m1=new Succubus(m0);
            m1.display();
            Morrigan m2=new Girl(m0);
            m2.display();
        }
    }
    //抽象构件角色：莫莉卡
    interface  Morrigan
    {
        public void display();
    }
    //具体构件角色：原身
    class original extends JFrame implements Morrigan
    {
        private static final long serialVersionUID = 1L;
        private String t="Morrigan0.jpg";
        public original()
        {
            super("《恶魔战士》中的莫莉卡·安斯兰");                
        }
        public void setImage(String t)
        {
            this.t=t;           
        }
        public void display()
        {   
            this.setLayout(new FlowLayout());
            JLabel l1=new JLabel(new ImageIcon("src/decorator/"+t));
            this.add(l1);   
            this.pack();       
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
            this.setVisible(true);
        }
    }
    //抽象装饰角色：变形
    class Changer implements Morrigan
    {
        Morrigan m;   
        public Changer(Morrigan m)
        {
            this.m=m;
        }   
        public void display()
        {
            m.display();
        }
    }
    //具体装饰角色：女妖
    class Succubus extends Changer
    {
        public Succubus(Morrigan m)
        {
            super(m);
        }   
        public void display()
        {
            setChanger();
            super.display();   
        }
        public void setChanger()
        {
            ((original) super.m).setImage("Morrigan1.jpg");           
        }
    }
    //具体装饰角色：少女
    class Girl extends Changer
    {
        public Girl(Morrigan m)
        {
            super(m);
        }   
        public void display()
        {
            setChanger();
            super.display();   
        }
        public void setChanger()
        {
            ((original) super.m).setImage("Morrigan2.jpg");           
        }
    }
```
![变身](变身.gif)

### 装饰模式的应用场景
前面讲解了关于装饰模式的结构与特点，下面介绍其适用的应用场景，装饰模式通常在以下几种情况使用。
- 当需要给一个现有类添加附加职责，而又不能采用生成子类的方法进行扩充时。例如，该类被隐藏或者该类是终极类或者采用继承方式会产生大量的子类。
- 当需要通过对现有的一组基本功能进行排列组合而产生非常多的功能时，采用继承关系很难实现，而采用装饰模式却很好实现。
- 当对象的功能要求可以动态地添加，也可以再动态地撤销时。

装饰模式在 Java 语言中的最著名的应用莫过于 Java I/O 标准库的设计了。例如，InputStream 的子类 FilterInputStream，OutputStream 的子类 FilterOutputStream，Reader 的子类 BufferedReader 以及 FilterReader，还有 Writer 的子类 BufferedWriter、FilterWriter 以及 PrintWriter 等，它们都是抽象装饰类。

下面代码是为 FileReader 增加缓冲区而采用的装饰类 BufferedReader 的例子： 
```java
    BufferedReader in=new BufferedReader(new FileReader("filename.txtn));
    String s=in.readLine();
```
### 装饰模式的扩展
装饰模式所包含的 4 个角色不是任何时候都要存在的，在有些应用环境下模式是可以简化的，如以下两种情况。

(1) 如果只有一个具体构件而没有抽象构件时，可以让抽象装饰继承具体构件，其结构图如图 4 所示。
![只有一个具体构件的装饰模式](只有一个具体构件的装饰模式.gif)
(2) 如果只有一个具体装饰时，可以将抽象装饰和具体装饰合并，其结构图如图 5 所示。
![只有一个具体装饰的装饰模式](只有一个具体装饰的装饰模式.gif)

## CPP

摘要
本章主要说明装饰者模式，该设计模式主要意图是：动态的给一些对象添加一些额外的职责，相比于继承子类更加灵活。
　 　有的时候我们想给某个对象增加一些功能（而不是给整个类增加功能，不需要通过定义子类添加新的类实现），例如一个普通滑块增加立体效果，在一个普通样式增加花纹。
　 　具体实现下边我们就通过一个小栗子来说明什么是装饰者模式。
### 主要参与者
该设计模式的参与者有5个，分别是：
- Virtual Component 抽象对象的接口，可以给这些对象动态的添加职责
- Concrete Component 实例对象，可以给这个对象增加一些职责
- Virtual Decorator 维持一个指向Component对象的指针，并定义与Component接口一致的接口
- Concrete Decorator 具体向组件添加职责
- Client 用户

### 具体实现代码
例子中Cake是抽象鸡蛋灌饼接口，OriginalCake是不加任何配料的原味鸡蛋灌饼，Decorator是抽象装饰器，EggAddCake 是给当前饼再加一个鸡蛋，SausageAddCake 是给当前饼再加一个香肠，我们在买鸡蛋灌饼时可以选择什么都不加，或加一个蛋，加多个蛋，加肠，加多个肠，同时加蛋和肠的情况，如果使用继承的方式实现上述操作可能会造成产生很多类且比较死板，比如说加蛋一个类，加肠一个类，同时加蛋和肠一个类，如果有其他配菜则会更加复杂，而使用装饰者模式则是在原有实例对象的基础上增加修饰，接下来我们通过一个实例代码来说明具体实现：

对象的接口（Component）

```cpp
#ifndef CAKE_H
#define CAKE_H
#include <stdio.h>
class Cake
{
public:
    virtual void Info() = 0;
    virtual float Price() = 0;
};

class OriginalCake : public Cake
{
public:
    OriginalCake(){}
    void Info(){printf("Original test cake. ");}
    float Price(){return 3.0;}  //base price
};

#endif // CAKE_H
```
装饰器的接口（Decorator）
```cpp
#ifndef DECORATOR_H
#define DECORATOR_H
#include "cake.h"

class Decorator : public Cake
{
public:
    Decorator(Cake *cake = 0){
        this->Init(cake);
    }
    void Init(Cake *cake){
        if(0 == cake)
            this->m_cake = new OriginalCake;
        else
            this->m_cake = cake;
    }
    virtual void Info(){m_cake->Info();}
    virtual float Price(){return m_cake->Price();}
protected:
    Cake *m_cake;
};

class EggAddCake : public Decorator
{
public:
    EggAddCake(Cake *cake = 0){Init(cake);}
    void Info(){
        m_cake->Info();
        printf("Add Egg. ");}
    float Price(){return m_cake->Price() + 1.5;}
};

class SausageAddCake : public Decorator
{
public:
    SausageAddCake(Cake *cake = 0){Init(cake);}
    void Info(){
        m_cake->Info();
        printf("Add Sausage. ");}
    float Price(){return m_cake->Price() + 2.0;}
};

#endif // DECORATOR_H
```
用户（Client）
```cpp
#include <QCoreApplication>
#include "cake.h"
#include "decorator.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //原味鸡蛋灌饼
    OriginalCake *originalCake = new OriginalCake;
    originalCake->Info();
    printf("\n Price %.2f\n",originalCake->Price());
    //加蛋鸡蛋灌饼
    EggAddCake *eggAddCake = new EggAddCake(new OriginalCake);
    eggAddCake->Info();
    printf("\n Price %.2f\n",eggAddCake->Price());
    //加蛋加肠鸡蛋灌饼
    SausageAddCake *sausageAddCake = new SausageAddCake(new EggAddCake(new OriginalCake));
    sausageAddCake->Info();
    printf("\n Price %.2f\n",sausageAddCake->Price());
    //加肠鸡蛋灌饼
    SausageAddCake *sausageAddCake1 = new SausageAddCake();
    sausageAddCake1->Info();
    printf("\n Price %.2f\n",sausageAddCake1->Price());
    return a.exec();
}
```
![输出结果](输出结果.jpg)


--- 


```cpp
// Decorator.h
#ifndef _DECORATOR_H_
#define _DECORATOR_H_
class Component {
 public:
  virtual ~Component();
  virtual void Operation();

 protected:
  Component();

 private:
};
class ConcreteComponent : public Component {
 public:
  ConcreteComponent();
  ~ConcreteComponent();
  void Operation();

 protected:
 private:
};
class Decorator : public Component {
 public:
  Decorator(Component* com);
  virtual ~Decorator();
  void Operation();

 protected:
  Component* _com;

 private:
};
class ConcreteDecorator : public Decorator {
 public:
  ConcreteDecorator(Component* com);
  ~ConcreteDecorator();
  void Operation();
  void AddedBehavior();

 protected:
 private:
};
#endif  //~_DECORATOR_H_
```

```cpp
// Decorator.cpp
#include <iostream>

#include "Decorator.h"
Component::Component() {}
Component::~Component() {}
void Component::Operation() {}
ConcreteComponent::ConcreteComponent() {}
ConcreteComponent::~ConcreteComponent() {}
void ConcreteComponent::Operation(){std::cout << "ConcreteComponent
                                    operation... "<<std::endl;
} Decorator::Decorator(Component* com) {
  this->_com = com;
}
Decorator::~Decorator() { delete _com; }
void Decorator::Operation() {}
ConcreteDecorator::ConcreteDecorator(Compo nent* com) : Decorator(com) {}
ConcreteDecorator::~ConcreteDecorator() {}
void ConcreteDecorator::AddedBehavior() {
  std::cout << "ConcreteDecorator::AddedBe
               hacior...."<<std::endl;
}
void ConcreteDecorator::Operation() {
  _com->Operation();
  this->AddedBehavior();
}
```

```cpp
// main.cpp
#include <iostream>

#include "Decorator.h"
using namespace std;
int main(int argc, char* argv[]) {
  Component* com = new ConcreteComponent();
  Decorator* dec = new ConcreteDecorator(com);
  dec->Operation();
  delete dec;
  return 0;
}
```
